//
//  WebViewController.swift
//  beaconpayment
//
//  Created by Ирина on 28.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation


class WebViewController: BaseViewController, UIWebViewDelegate, NSURLConnectionDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    
    var urlString: String = ""
    
    override func viewDidLoad() {
        let url = URL(string: urlString)
        let urlRequest = NSURLRequest(url: url!)
        let urlConnection:NSURLConnection = NSURLConnection(request: urlRequest as URLRequest, delegate: self)!
        webView.loadRequest(urlRequest as URLRequest)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        popToRootIfNoUser()
        self.navigationController?.navigationBar.topItem?.title = "Прикрепление карты"
    }

    func connection(_ connection: NSURLConnection, canAuthenticateAgainstProtectionSpace protectionSpace: URLProtectionSpace) -> Bool {
        print("canAuthenticateAgainstProtectionSpace method Returning True")
        return true
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        dismissProgress()
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        showProgress(msg: "Загрузка...")
    }
    
    func connection(_ connection: NSURLConnection, didReceive challenge: URLAuthenticationChallenge){
        
        print("did autherntcationchallenge = \(challenge.protectionSpace.authenticationMethod)")
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust  {
            print("send credential Server Trust")
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            challenge.sender!.use(credential, for: challenge)
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic{
            print("send credential HTTP Basic")
            let defaultCredentials: URLCredential = URLCredential(user: "username", password: "password", persistence:URLCredential.Persistence.forSession)
            challenge.sender!.use(defaultCredentials, for: challenge)
            
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodNTLM{
            print("send credential NTLM")
            
        } else{
            challenge.sender!.performDefaultHandling!(for: challenge)
        }
    }
    
   func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        let url: String = request.url!.absoluteString
        print("loading url \(url)")
        if url.contains("returnurl"){
            ProfileViewController.needToApplyBinding = true
           // performSegue(withIdentifier: "HomeSegue", sender: nil)
          //  self.navigationController?.popToRootViewController(animated: true)
            self.navigationController?.popViewController(animated: true)
            return false
        }
        return true
        
    }
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        
    }
    


}
