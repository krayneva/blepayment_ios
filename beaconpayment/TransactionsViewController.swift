//
//  TransactionsViewController.swift
//  beaconpayment
//
//  Created by Ирина on 17.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//
import UIKit
//import Foundation


class TransactionsViewController: BaseViewController

, UITableViewDataSource, UITableViewDelegate
{

  

    @IBOutlet weak var ordersTable: UITableView!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var statusText: UILabel!
  

   
    var ordersList: [Order]  = [Order]()
    var currentPage = 0
    var totalPages = 1
    var currentOrder = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        addSwipeRecognizers()
        ordersTable.isHidden = true
        showMoreButton.isHidden = true
    
        NotificationCenter.default.addObserver(self, selector: #selector(updateLog(notification:)), name: NSNotification.Name(rawValue: BleHandler.logNotificationId), object: nil)
    }
    
    
    @IBAction func showMoreButtonTapped(_ sender: Any) {
        loadPayments()
    }

    
    
    override func initUI(){
        super.initUI()
        popToRootIfNoUser()
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.topItem?.title = "История"
        updateOrdersList()
        if (ConnectionListener.sharedInstance.isConnected()){
            if (currentPage == 0){
                loadPayments()
            }
        }
        else{
            showToast("Нет подключения к интернету")
        }
      //  logView.text = ""

    }
    
    
    func updateLog(notification: NSNotification){
      /*  if let message = notification.userInfo?["message"] as? String{
           // logView.text.append(other: message+"\n")
            logView.text = logView.text.appending(message+"\n")
        }*/
    }

    
    
    override func clearDataOnDismiss() {
        ordersList.removeAll()
        currentPage = 0
        totalPages = 1
        currentOrder = 0
    }


    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentOrder = indexPath.row
      //  if ordersList[currentOrder].valid_address{
            performSegue(withIdentifier: "ShowCheckSegue", sender: nil)
      //  }
    }

   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return ordersList.count
    }
    
    func loadPayments(){
        let helper = RequestHelper()
        showMoreButton.isHidden = true
        showProgress(msg: "Загрузка списка платежей...")
        helper.requestOrders(User.getField(User.Fields.ID) as! Int, pageNum: currentPage+1, listener: self)
        ordersTable.reloadData()
    }
    
    @IBAction func onMapClick(_ sender: Any) {
        performSegue(withIdentifier: "ShowMapSegue", sender: nil)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",
                                                               for: indexPath) as! TransactionRowView
        if let order = ordersList[indexPath.row] as Order? {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let d = Date(timeIntervalSince1970: TimeInterval(order.date))
            cell.timeText.text = dateFormatter.string(from: d)
            dateFormatter.dateFormat = "dd.MM.yy"
            cell.dateText.text = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(order.date)))
            cell.merchantText.text = order.merchant
            cell.amountText.text = CurrencyAmountUtils.getReadableAmount(currency: order.currency, amount: order.amount)
          //  cell.amountText.text = String(order.amount)
            cell.statusText.text = order.getStatus().name
            cell.statusText.textColor = order.getStatus().color
            cell.amountText.textColor = order.getStatus().color
            cell.geoButton.isHidden = !order.valid_address

        }
        return cell
    }
    

    
    func updateOrdersList(){
        ordersTable.reloadData()
        showMoreButton.isHidden = (currentPage==totalPages)
        ordersTable.isHidden = ordersList.isEmpty
        statusText.isHidden = !ordersList.isEmpty
    }
    
    
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        dismissProgress()
        switch requestType {
        case RequestHelper.RequestType.getOrders:
            if let orders =  data?["orders"] as? NSArray{
                for item in orders
                {
                    if  let order = Order(dict: item as! Dictionary<String, AnyObject>) as Order? {
                        ordersList.append(order)
                    }
                }
            }
            totalPages = data?["total_pages"] as! Int
            currentPage = data?["page_number"] as! Int
            currentPage = currentPage>totalPages ? totalPages : currentPage
            updateOrdersList()
        default:
            return
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let order = ordersList[currentOrder]
        if segue.identifier == "ShowMapSegue"{
            if let destinationVC = segue.destination as? MapControllerView {
                destinationVC.lat = order.lat
                destinationVC.lon = order.lon
            }
        }
        if segue.identifier == "ShowCheckSegue"{
            if let destinationVC = segue.destination as? CheckViewController {
                destinationVC.order = order
            }
        }
        
    }
    
    
    
   
    override func onConnectivityChange(connected: Bool) {
        showMoreButton.isEnabled = connected
        //TODO emptydataset!!!
    }
}
