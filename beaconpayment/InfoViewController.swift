//
//  InfoViewCotroller.swift
//  beaconpayment
//
//  Created by Ирина on 24.01.17.
//  Copyright © 2017 sbt. All rights reserved.
//

import Foundation

class InfoViewController: BaseViewController{
    @IBOutlet weak var versionLabel: UILabel!
    @IBAction func onAgreementClick(_ sender: UIButton) {
        performSegue(withIdentifier: "AgreementSegue", sender: nil)
    }
    
    override func initUI(){
        super.initUI()
        self.navigationController?.navigationBar.topItem?.title = "О программе"
        addSwipeRecognizers()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            versionLabel.text = "Версия \(version)"
        }
        else{
            versionLabel.text = ""
        }
    }
}
