//
//  CheckViewController.swift
//  beaconpayment
//
//  Created by Ирина on 23.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//

import Foundation
class CheckViewController: BaseViewController{
    
    var order: Order!
    
    
    @IBOutlet weak var authDateLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var merchantIdLabel: UILabel!
    @IBOutlet weak var posIdLabel: UILabel!
    @IBOutlet weak var terminalIdLabel: UILabel!
    @IBOutlet weak var approvalCodeLabel: UILabel!
    @IBOutlet weak var merchantOrderLabel: UILabel!
    @IBOutlet weak var panLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Чек"
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    
    
    override func initUI(){
        super.initUI()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm dd.MM.yy"
        let d = Date(timeIntervalSince1970: TimeInterval(order.date))

        authDateLabel.text = "Дата платежа: \(dateFormatter.string(from: d))"
        merchantNameLabel.text = "Продавец: \(order.merchant)"
        merchantIdLabel.text = "Номер продавца: \(order.merchant)"
        posIdLabel.text = "Номер кассы: \(order.pos_terminal_id)"
        terminalIdLabel.text = "Терминал: \(order.terminal_id)"
        approvalCodeLabel.text = "Код авторизации: \(order.approval_code)"
        merchantOrderLabel.text = "Номер заказа: \(order.merchant_order_number)"
        panLabel.text = "Номер карты: \(order.pan)"
        amountLabel.text = "Сумма заказа: \(CurrencyAmountUtils.getReadableAmountWithRemaider(currency:order.currency,amount:order.amount)!)"
    }
    
    
    
   
}
