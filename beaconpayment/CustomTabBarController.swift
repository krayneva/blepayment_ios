//
//  CustomTabBarController.swift
//  beaconpayment
//
//  Created by Ирина on 22.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit



class CustomTabBarController:UITabBarController, UITabBarControllerDelegate{
    
    var currentIndex = 0

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.delegate = self
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabViewControllers = tabBarController.viewControllers!
        guard let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        animateToTab(toIndex: toIndex)
        currentIndex = toIndex
        return true
    }
    
    func animateToTab(toIndex: Int) {
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController!.view
        let fromIndex = tabViewControllers.index(of: selectedViewController!)

        let scrollRight = toIndex > fromIndex!;
        if currentIndex == 4{
            currentIndex = 0
        }
        if currentIndex == -1{
            currentIndex = 3
        }
        
        // clearing temp data
        if let controller = selectedViewController as? BaseViewController{
            controller.clearDataOnDismiss()
        }
        
        let toView = tabViewControllers[currentIndex].view

        guard fromIndex != currentIndex else {return}
        
        // Add the toView to the tab bar view
        fromView?.superview!.addSubview(toView!)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: (fromView?.center.x)! + offset, y: (toView?.center.y)!)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView?.center = CGPoint(x: (fromView?.center.x)! - offset, y: (fromView?.center.y)!);
            toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!);
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView?.removeFromSuperview()
            self.selectedIndex = self.currentIndex
            self.view.isUserInteractionEnabled = true
        })
    }

    
    func onSwipeRight(_ sender: Any) {
        currentIndex -= 1
        animateToTab(toIndex: currentIndex)
    }
    
    func onSwipeLeft(_ sender: Any) {
        currentIndex += 1
        animateToTab(toIndex: currentIndex)
    }
    
    func initUI(showAllTabs: Bool) {
        self.tabBar.items?[1].isEnabled = showAllTabs
        self.tabBar.items?[2].isEnabled = showAllTabs
        self.tabBar.items?[3].isEnabled = showAllTabs
    }
    
}
