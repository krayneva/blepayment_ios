//
//  CustomButton.swift
//  beaconpayment
//
//  Created by Ирина on 30.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit



@IBDesignable class CustomButton: UIButton{
    
    //TODO: enabled & disabled colors --> to storyboard
    @IBInspectable var bgColorEnabled: UIColor = .white {
        didSet {
            setupButton()
        }
    }
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        self.setBackgroundColor(Colors.main.get(), state: UIControlState.normal)
        self.setBackgroundColor(Colors.darkGray.get(), state: UIControlState.disabled)
        self.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        self.contentEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
        
       // self.backgroundColor = bgColorEnabled
    }
}
extension  UIButton {
    
    func setBackgroundColor(_ color: UIColor, state: UIControlState){
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let con  = UIGraphicsGetCurrentContext()
        con?.setFillColor(color.cgColor)
        con?.fill(CGRect(x:0,y:0,width:1, height:1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        self.setBackgroundImage(colorImage, for: state)
        
    }
}
