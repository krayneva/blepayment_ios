//
//  ViewController.swift
//  beaconpayment
//
//  Created by Ирина on 05.08.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import UIKit
import MobileCoreServices
import ImagePicker

class RegistrationViewController: BaseViewController
    , ImagePickerDelegate
    , UITableViewDelegate
    , UITableViewDataSource
    
{
  
    
    @IBOutlet weak var surnameEdit: UITextField!
    @IBOutlet weak var nameEdit: UITextField!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var deletePhotoButton: UIButton!
    @IBOutlet weak var thumbsTable: UITableView!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var addPhotoLabel: UILabel!

    @IBAction func onAddPhotoClick(_ sender: Any) {
        showCamera()
    }
    
   
    
    //  var scrollDelegate:ScroollViewDelegate?
    var photoPaths = [String]()
    var currentPhoto = 0
    let imagePicker = ImagePickerController()
    

    
    @IBAction func photoImageTapped(_ sender: UITapGestureRecognizer) {
        showCamera()
    }
    
    
    func showCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.imageLimit = 10
            imagePicker.galleryView.isHidden = false
            imagePicker.resetAssets()
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion:nil)
        }
        else{
            showToast("Камера не найдена")
        }

    }
    
    

    
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
        for image in images{
            let photoPath = ImageUtils.resizeImage(image: image)
            User.photosToAdd.append(photoPath)
        }
        currentPhoto = photoPaths.count
        setPhotoImages()
    }
    
    //callbacks from camera
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {}
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {}
    
    
    override func initUI(){
        super.initUI()
        setPhotoImages()
        self.navigationController?.navigationBar.topItem?.title = "Регистирация"
        surnameEdit.isEnabled = true
        nameEdit.isEnabled = true
        phoneLabel.text = "+7"+SignInData.phone
        photoImage.isUserInteractionEnabled = true
    }

    
    
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        let name = nameEdit.text!
        let surname = surnameEdit.text!
        let phone = SignInData.phone
    
        if !validateData(name: name, surname: surname) {
            showToast("Введите корректные данные")
            return
        }
    let helper = RequestHelper()
        showProgress(msg: "Регистрация пользователя...")
        helper.addUser(name: name, surname: surname, phone: "+7\(phone)", listener: self)
    }



    
    
    
    func validateData(name:String, surname:String)->Bool{
        if name.isEmpty || surname.isEmpty || photoPaths.isEmpty{
            return false
        }
        return true
    }
    
    
    override func updateUIErrorReceived(requestType: RequestHelper.RequestType, error: ErrorResponse!) {
        super.updateUIErrorReceived(requestType: requestType, error: error)
         let userID = User.getField(User.Fields.ID) as! Int
        switch requestType{                                                                                                                                                                                                                                                                                                                                                
        case .addPhoto:
            // define which photo is bad
            User.photosToAdd.remove(at: 0)
            initUI()
            showProgress(msg: "Сохранение фотографий..")
            RequestHelper().addAndRemoveUserPhotos(userID, listener: self)
        default:
            break
        }
    }
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        switch requestType{
       // case .addPhoto:
            
        case .addUser:
            let userID = User.getField(User.Fields.ID) as! Int
            showProgress(msg: "Сохранение фотографий...")
            RequestHelper().addAndRemoveUserPhotos(userID,listener: self)
        case .editUser:
            dismissProgress()
            initUI()
        case .loadPhoto:
            dismissProgress()
            BleHandler.sharedInstance.startScanning()
            performSegue(withIdentifier: "HomeSegue", sender: nil)
        default: break
        }
    }
    
    func setPhotoImages(){
        photoPaths.removeAll()
        let photos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        for  photo in photos {
            photoPaths.append(photo.fullPathURL!.path)
        }
        User.photosToAdd = Array(Set(User.photosToAdd).subtracting(User.photosToDelete))
        photoPaths.append(contentsOf: User.photosToAdd)
        photoPaths = Array(Set(photoPaths).subtracting(User.photosToDelete))
        setMainPhoto(newCurrentPhotoIndex: currentPhoto)
        setThumbnaiuls()
        
        }
    
    func setMainPhoto(newCurrentPhotoIndex: Int){
        addPhotoButton.isHidden = !photoPaths.isEmpty
        addPhotoLabel.isHidden = !photoPaths.isEmpty
        deletePhotoButton.isHidden = photoPaths.isEmpty
        if !photoPaths.isEmpty{
            currentPhoto = newCurrentPhotoIndex
            if currentPhoto == -1 && photoPaths.count>0 {
                currentPhoto = 0
            }
            if photoPaths.count>=currentPhoto {
                photoImage.image = UIImage(named: photoPaths[currentPhoto])
            }
            else{
              
            }
        }
    }
    
    @IBAction func deletePhotoButtonClicked(_ sender: UIButton) {
        if photoPaths.count>1{
            let path = photoPaths[currentPhoto]
            User.photosToDelete.append(path)
            ImageUtils.deleteFile(path: path)
            currentPhoto -= 1;
            setPhotoImages()
        }
        else{
            showToast("Нельзя удалить последнее фото")
        }
    }
    
    

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThumbCell", for: indexPath) as! ImageThumbnailRowView
        cell.imageThumb.image = UIImage(named: photoPaths[indexPath.row])
        cell.imageThumb.contentMode = .scaleAspectFit
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setMainPhoto(newCurrentPhotoIndex: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return photoPaths.count
    }
    
    func setThumbnaiuls(){
        thumbsTable.reloadData()
    }
    
    
    override func clearDataOnDismiss() {
        print("clearing signindata, user photos to send and to delete")
        User.photosToDelete.removeAll()
        User.photosToAdd.removeAll()
        for path in User.photosToAdd{
            ImageUtils.deleteFile(path: path)
        }
        currentPhoto = 0
    }

    
    override func onConnectivityChange(connected: Bool) {
        saveButton.isEnabled = connected
    }
}

