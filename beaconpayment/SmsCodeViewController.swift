//
//  SmsCodeViewController.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit

class SmsCodeViewController :BaseViewController{
    @IBOutlet weak var codeEdit: UITextField!
    
    @IBOutlet weak var okSmsButton: UIButton!
    

    override func initUI(){
        super.initUI()
        codeEdit.text = ""
        if (ConnectionListener.sharedInstance.isConnected()){
            showProgress(msg: "Запрос смс кода...")
            let helper = RequestHelper()
            helper.getSms(SignInData.userID, phone: SignInData.phone, registered: SignInData.registered , listener: self)
        }
        else{
            showToast("Нет подключения к интернету")
        }
    }
  
    
    @IBAction func okSmsButtonClick(_ sender: AnyObject) {
       let helper = RequestHelper()
        let code = codeEdit.text!
        if !code.isEmpty{
            let smsCode = Int(code)
            showProgress(msg: "Проверка смс кода...")
            helper.checkSmsCode(sms_id: SignInData.smsID, code: smsCode!, registered: SignInData.registered, listener: self)
        }
        else {
            showToast("Введите смс-код")
        }
    }
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {

        switch requestType{
            case RequestHelper.RequestType.checkSmsCode:
                if (SignInData.registered){
                    dismissProgress()
                    showProgress(msg: "Загрузка фотографий...")
                    RequestHelper().loadUserPhotoURLs(User.getField(User.Fields.ID) as! Int, listener: self)
                }
                else{
                    dismissProgress()

                      performSegue(withIdentifier: "AgreementSegue", sender: nil)
                }
        case .getSms:
                dismissProgress()
            case RequestHelper.RequestType.getPhotoURLs:
                RequestHelper().loadPhotos(User.getField(User.Fields.ID) as! Int, listener: self)
            case RequestHelper.RequestType.loadPhoto:
                BleHandler.sharedInstance.startScanning()
                dismissProgress()
                performSegue(withIdentifier: "HomeSegue", sender: nil)
                
            default: break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        codeEdit.text = ""
    }

    override func clearDataOnDismiss() {
        print("clearing signindata, userId and smsId and registered")
        SignInData.smsID = ""
        SignInData.userID = 0
        SignInData.registered = false
    }
    
    override func onConnectivityChange(connected: Bool) {
        okSmsButton.isEnabled = connected
    }
}
