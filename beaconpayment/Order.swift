//
//  Order.swift
//  beaconpayment
//
//  Created by Ирина on 17.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit

class Order{
    let date: TimeInterval
    let amount: Int
    let currency: Int
    let status: Int
    let merchant:String
    let merchant_address:String
    
    let merchant_id:Int
    let pos_terminal_id:Int
    let terminal_id:String
    let approval_code:String
    let merchant_order_number:String
    let pan:String

    
    
    var valid_address = true
    var lat = 0.0, lon = 0.0
    
    init?(dict: Dictionary<String, AnyObject>){
        date = (dict["date"] as? TimeInterval ?? 0)/1000.0
        amount = (dict["amount"] as? Int ?? 0)
        currency = dict["currency"] as? Int ?? 643
        merchant = dict["merchant_name"] as? String ?? "Не задан"
        status = dict["status"] as? Int ?? 0
        
        merchant_id = dict["merchant_id"] as? Int ?? 0
        pos_terminal_id = dict["pos_terminal_id"] as? Int ?? 0
        terminal_id = dict["terminal_id"] as? String ?? ""
        approval_code = dict["approval_code"] as? String ?? ""
        pan = dict["pan"] as? String ?? ""
        merchant_order_number = dict["merchant_order_number"] as? String ?? ""
        merchant_address = dict["merchant_address"] as? String ?? ""
        let geo = merchant_address.components(separatedBy: ",")
        if geo.count==2{
            valid_address = true
            lat = (geo[0] as NSString).doubleValue
            lon = (geo[1] as NSString).doubleValue
        }
        else{
            valid_address = false
        }

    }
    
    typealias Status = (color: UIColor, name: String)
    
    func getStatus()->Status{
        switch status{
        case 1:
            return (Colors.orderRegistered.get(), "Зарегистрирован")
        case 2:
            return (Colors.orderSuccess.get(), "Успешно")
        case 3:
            return (Colors.orderError.get(), "Ошибка")
        case 4:
            return (Colors.orderCancelled.get(), "Отменен")
        default:
            return (UIColor.black,"Статус неизвестен")
        }
    }
    
    
}
