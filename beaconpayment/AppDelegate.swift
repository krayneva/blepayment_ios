//
//  AppDelegate.swift
//  beaconpayment
//
//  Created by Ирина on 05.08.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import UIKit
import CoreLocation
import SystemConfiguration
import Foundation
import HockeySDK
import Firebase
import UserNotifications
import UserNotificationsUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,
//CLLocationManagerDelegate,
UNUserNotificationCenterDelegate, FIRMessagingDelegate, UpdateUIProtocol, URLSessionDelegate, NSURLConnectionDelegate{
    
  /*  lazy var downloadsSession: URLSession = {
      //  let conf = URLSessionConfiguration.background(withIdentifier: "backID")
        let conf = URLSessionConfiguration.default
        let session = URLSession(configuration: conf, delegate: self, delegateQueue: nil)
        return session
    }()*/
    var window: UIWindow?
    var timer : Timer?
        var currentBgTaskId : UIBackgroundTaskIdentifier?
    
    let bleHandler = BleHandler.sharedInstance
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        BITHockeyManager.shared().configure(withIdentifier: "c5bdd54514b34e3389ee94fea529b97a")
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation() // This line is obsolete in the crash only builds
        bleHandler.startScanning()
        bleHandler.startUIUpdate()
        ConnectionListener.sharedInstance.startMonitoringConnection()
        FIRApp.configure()
        // [START add_token_refresh_observer]
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        // [END add_token_refresh_observer]
       // _ = self.downloadsSession
        return true
    }
    
    /// The callback to handle data message received via FCM for devices running iOS 10 or above.
    public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print ("remote message received: \(remoteMessage)")
        print("notification func 1")
         RequestHelper().logToServer("app received push 1")
        BleHandler.sharedInstance.sendAliveOrAllIn()
    }
    
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        RequestHelper().logToServer("app received push 2")

        application.registerForRemoteNotifications()
        BleHandler.sharedInstance.sendAliveOrAllIn()
        
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("fetch Complete");
        BleHandler.sharedInstance.sendAliveOrAllIn()
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        let userID = User.getField(User.Fields.ID) as! Int
        
        if userID>0 {
            RequestHelper().addUserDevice(userID,listener: self)
        }
        
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("notification func 3")
        BleHandler.sharedInstance.sendAliveOrAllIn()
         RequestHelper().logToServer("app received push 3")
    }
    
  /*8  var backgroundTask:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    
    let serverAddr = "https://52.50.183.84/sberbank/api/v1/contactless"
    
    enum Urls: String{
        case user = "/users"
        case checkPhone = "/auth/check"
        case getSms = "/auth/sms/send"
        case checkSmsCode = "/auth/sms/verify"
        case photos = "/photos"
        case events = "/events"
        case merchants = "/merchants/find"
        case orders = "/orders"
        case photoAuth = "/auth/photo"
        case bindingsRegistration = "/bindings/registration"
        case bindings = "/bindings"
        case devices = "/devices"
        case log = "/push/test"
    }*/

    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("---\(userInfo)")
        print("received notification!")
        BleHandler.sharedInstance.locationManager.startUpdatingLocation()
        beginNewBackgroundTask()
        completionHandler(UIBackgroundFetchResult.newData)
    }

    
    func beginNewBackgroundTask(){
          print("-------------app delegate started background task")
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("task expired: ")
        })
        if let taskId = previousTaskId{
            UIApplication.shared.endBackgroundTask(taskId)
            previousTaskId = UIBackgroundTaskInvalid
        }
        timer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(self.stopUpdatingLocation),userInfo: nil, repeats: false)
    }
    
    func stopUpdatingLocation(){
        print("--------------app delegate stopped updating location")
        timer?.invalidate()
        timer = nil
        BleHandler.sharedInstance.locationManager.stopUpdatingLocation()
    }
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        print("handle called")
        
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // application.setMinimumBackgroundFetchInterval(NSTimeInterval.init(1))
        BITHockeyManager.shared().updateManager.checkForUpdate()
        bleHandler.stopUIUpdate()
        ConnectionListener.sharedInstance.stopMonitoringConnection()
    }
    
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        bleHandler.turnOnBluetoothIfNeeded()
        bleHandler.startUIUpdate()
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        ConnectionListener.sharedInstance.startMonitoringConnection()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        bleHandler.stopScanning()
        SignInData.clearData()
        RequestHelper().logToServer("app is about to terminate!")

        
    }
    
    
    func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?){}
    func updateUIErrorReceived(requestType: RequestHelper.RequestType, error:ErrorResponse!){}

    
}

