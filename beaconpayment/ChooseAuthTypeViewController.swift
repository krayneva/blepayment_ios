//
//  ChooseAuthTypeViewController.swift
//  beaconpayment
//
//  Created by Ирина on 24.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation

class ChooseAuthTypeViewController: BaseViewController{
  
    @IBOutlet weak var photoButton: CustomButton!
    @IBOutlet weak var smsCodeButton: CustomButton!
    
    
    @IBAction func photoAuthTapped(_ sender: Any) {
      performSegue(withIdentifier: "PhotoAuthSegue", sender: nil)
    }

    @IBAction func smsAuthTapped(_ sender: Any) {
         performSegue(withIdentifier: "SmsAuthSegue", sender: nil)
    }
    
    override func clearDataOnDismiss() {
        print("clearing signindata, userId, registered")
        SignInData.userID = 0
        SignInData.registered = false
    }

    
    override func onConnectivityChange(connected: Bool) {
        photoButton.isEnabled = connected
        smsCodeButton.isEnabled = connected
    }
}
