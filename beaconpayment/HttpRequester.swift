//
//  ServerConnector.swift
//  beaconpayment
//
//  Created by Ирина on 11.08.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit

class HttpRequester: NSObject,URLSessionDelegate, URLSessionTaskDelegate {
    let TAG = "HttpRequester "
    static let popToRootNotificationId = "popToRootNotification"

   
        
    //works
    func requestGet(_ url: String, requestType:RequestHelper.RequestType, listener:UpdateUIProtocol?) {
        print(TAG,"request get type "+requestType.rawValue,url)
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod="GET"
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("utf-8",forHTTPHeaderField: "Content-encoding")

        executeRequest(request: request,requestType: requestType,listener: listener)
    }
    
    func requestDelete(_ url:String, requestType:RequestHelper.RequestType, listener:UpdateUIProtocol?) {
        print(TAG,"request delete type "+requestType.rawValue,url)
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod="DELETE"
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("utf-8",forHTTPHeaderField: "Content-encoding")
        executeRequest(request: request,requestType: requestType,listener: listener)
    }
    
    func requestPut(_ url: String,  requestBody: Dictionary<String, AnyObject>,requestType:RequestHelper.RequestType, listener:UpdateUIProtocol?){
        print(TAG,"request put type "+requestType.rawValue,url)
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod="PUT"
        request.httpBody = dictToJSONString(requestBody)
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("utf-8",forHTTPHeaderField: "Content-encoding")
        print(TAG,"\(requestType.rawValue) request body"+(NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue) as! String))
        executeRequest(request: request,requestType: requestType,listener: listener)
    }
    
    
    func requestPost(_ url: String,  requestBody: Dictionary<String, AnyObject>,requestType:RequestHelper.RequestType, listener:UpdateUIProtocol?){
        print(TAG,"request post type "+requestType.rawValue,url)
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod="POST"
        request.httpBody = dictToJSONString(requestBody)
        request.addValue("application/json",forHTTPHeaderField: "Content-Type")
        request.addValue("utf-8",forHTTPHeaderField: "Content-encoding")
        print(TAG,"\(requestType.rawValue) request body"+(NSString(data: request.httpBody!, encoding: String.Encoding.utf8.rawValue) as! String))
        executeRequest(request: request,requestType: requestType,listener: listener)
    }
    
    func requestUpload(_ url: String, data: UIImage, requestType: RequestHelper.RequestType, listener: UpdateUIProtocol?){
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        var builder = MultipartDataBuilder()
        builder.appendFormData(name: "photo",
                               content: UIImagePNGRepresentation(data)! as NSData,
                               fileName: "photo.png",
                               contentType: "application/octet-stream")
        
        request.setMultipartBody(data: builder.build()!, boundary: builder.boundary)
        
      //  request.addValue("multipart/form-data",forHTTPHeaderField: "Content-Type")
        print(TAG,"\(requestType.rawValue) request url is"+url)
        executeRequest(request: request, requestType: requestType, listener: listener)
    }
    
    
    func requestUpload(_ url: String, path:String, requestType: RequestHelper.RequestType, listener: UpdateUIProtocol?){
        let request = NSMutableURLRequest(url: URL(string: url)!)
      //  let imageData = UIImagePNGRepresentation(UIImage(named: path)!)
        let imageData = NSData(contentsOfFile: path)
        request.httpMethod = "POST"
        var builder = MultipartDataBuilder()
        builder.appendFormData(name: "photo",
                               content: imageData!,
                               fileName: "photo.png",
                               contentType: "application/octet-stream")
        if (requestType == .photoAuth){
            builder.appendFormData(key: Device.Fields.ID, value: Device().id)
            builder.appendFormData(key: Device.Fields.pushKey, value: Device().pushKey!)
            builder.appendFormData(key: Device.Fields.osType, value: Device().osType)
        }
        
        request.setMultipartBody(data: builder.build()!, boundary: builder.boundary)
        
        //  request.addValue("multipart/form-data",forHTTPHeaderField: "Content-Type")
        print(TAG,"\(requestType.rawValue) request url is"+url)
        executeRequest(request: request, requestType: requestType, listener: listener)
    }
    
    
    struct SessionProperties {
        static let identifier:String! = "url_session_background_download"
    }
    

    
    func executeRequest(request: NSMutableURLRequest, requestType:RequestHelper.RequestType, listener:UpdateUIProtocol?){
        print("execute request started")

        let token = User.getField(User.Fields.TOKEN) as! String
        if (!token.isEmpty){
             request.addValue("Bearer "+token,forHTTPHeaderField: "Authorization")
        }
        request.addValue(Device().id, forHTTPHeaderField: "X-Device-ID")

        let configuration = URLSessionConfiguration.default
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)

       let t =  session.dataTask(with: request as URLRequest) { (data:Data?, response:URLResponse?, error:Error?) in
            if let data = data{
                print("received response to Http")
                if let response = response as? HTTPURLResponse {
                    //if  200...299 ~= response.statusCode{
                    if response.statusCode != 401{
                        print ("http status is \(response.statusCode)")
                        ResponseParser().parseResult(data, request: request, requestType: requestType, listener: listener)
                    }
                    else{
                        //TODO: move error handling here
                        print ("http status is \(response.statusCode)")
                        ResponseParser().parseResult(data, request: request, requestType: requestType, listener: listener)
                        User.clearData()
                        BleHandler.sharedInstance.stopScanning()
                        SignInData.clearData()
                        NotificationCenter.default.post(name: Notification.Name(rawValue: HttpRequester.popToRootNotificationId), object: nil,userInfo: ["message": "did enter region"])

                      /*  if let listener = listener{
                            listener.updateUIPopToRoot()
                        }*/
                    }
                }
                else{
                   print ("cannot retrieve response as HHTPURLResponse") 
                }
            }
        }
        t.resume()
    }
    
    
    



    
    func urlSession(_ session: URLSession,
                    didReceive challenge: URLAuthenticationChallenge,
                                        completionHandler: @escaping (URLSession.AuthChallengeDisposition,
        URLCredential?) -> Void) {
        completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: (challenge.protectionSpace.serverTrust)!))
    }
    
    
    func dictToJSONString(_ dict: Dictionary<String, AnyObject>) -> Data{
        var jsonObject: Data = Data()
        do{
            
            jsonObject = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            
        }
        catch{
            print(error)
        }
        return jsonObject
    }

  }
