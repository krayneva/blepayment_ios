//
//  CurrenceAmountUtils.swift
//  beaconpayment
//
//  Created by Ирина on 23.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//

import Foundation


class CurrencyAmountUtils{
    
    
    static func getReadableAmount(currency: Int, amount: Int)->String?{
        return getSymbolForCurrencyCode(code: currency)!+getFormattedAmount(amount: amount)!
    }
    
    
    static func getReadableAmountWithRemaider(currency: Int, amount: Int)->String?{
        return getSymbolForCurrencyCode(code: currency)!+getFormattedAmountWithRemainder(amount: amount)!
    }
    
    
    static func getSymbolForCurrencyCode(code: Int) -> String?{
        //   let stringCode = IsoCountryCodes.find(key: "\(code)").currency
        let currency = ISO4217.Currency(rawValue: Int16(code))
        
        let stringCode = currency?.code
        if stringCode=="RUB"{
            return "\u{20BD}"
        }
        else{
            let locale = NSLocale(localeIdentifier: stringCode!)
            let symbol = locale.displayName(forKey: NSLocale.Key.currencySymbol, value:stringCode!)
            return symbol
        }
    }
    
    
    static func getFormattedAmount(amount: Int)->String?{
        let remainder = amount % 100
        let s: String?
        if (remainder==0){
            s = String(amount/100)
        }
        else{
            s = "\(amount/100).\(remainder)";
        }
        return s;
    }

    
    static func getFormattedAmountWithRemainder(amount: Int)->String?{
        let remainder = amount % 100
        let s: String?
        if (remainder==0){
            s = "\(amount/100).00";
        }
        else{
            s = "\(amount/100).\(remainder)";
        }
        return s;
    }
}
