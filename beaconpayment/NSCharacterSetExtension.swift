//
//  NSCharacterSetExtension.swift
//  beaconpayment
//
//  Created by Ирина on 25.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
extension NSCharacterSet {
    
    class func MIMECharacterSet() -> NSCharacterSet {
        let characterSet = NSCharacterSet(charactersIn: "\"\n\r")
        return characterSet.inverted as NSCharacterSet
    }
}
