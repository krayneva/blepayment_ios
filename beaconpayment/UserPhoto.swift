//
//  UserPhoto.swift
//  beaconpayment
//
//  Created by Ирина on 23.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation


class UserPhoto: NSObject, NSCoding {


    var url: URL?
    var fileName: String?
    var loaded: Bool = false
    var fullPathURL: URL?
    var id: Int?
    
    override init(){
        
    }
    
    init(url: String, fileName: String, id: Int) {
        self.url = URL(string: url)!
        self.fileName = fileName
        self.id = id
        fullPathURL = ImageUtils.getFullPath(fileName: fileName)
        let fileManager : FileManager   = FileManager.default
        loaded = fileManager.fileExists(atPath:(fullPathURL?.path)!)
        //isLoaded()
    }
    
    func isLoaded()->Bool{
        let fileManager : FileManager   = FileManager.default
        loaded = fileManager.fileExists(atPath:(fullPathURL?.path)!)
        return loaded
    }
    
    
    
    required convenience init?(coder decoder: NSCoder) {
        self.init()
        url = decoder.decodeObject(forKey: "url") as? URL
        fileName = decoder.decodeObject(forKey: "fileName") as? String
        id = decoder.decodeObject(forKey:"id") as? Int
        fullPathURL = ImageUtils.getFullPath(fileName: fileName!)
        let fileManager : FileManager   = FileManager.default
        loaded = fileManager.fileExists(atPath:(fullPathURL?.path)!)
    }
    

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: "url")
        aCoder.encode(fileName, forKey: "fileName")
        aCoder.encode(id, forKey:"id")

    }
    
   static func archivePhotos(photos:[UserPhoto]) -> NSData {
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: photos as NSArray)
        return archivedObject as NSData
    }
    
    
   static  func unarchivePhoto(data: NSData)-> [UserPhoto]{
        let photos = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [UserPhoto]
        return photos!
    }
    
}
