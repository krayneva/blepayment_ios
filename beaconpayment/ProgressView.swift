//
//  ProgressView.swift
//  beaconpayment
//
//  Created by Ирина on 30.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import Toast_Swift


public extension UIView{
    

    
    func progressView(message: String) -> UIView {
        var messageLabel: UILabel
        var progress: UIActivityIndicatorView
        let style = ToastManager.shared.style
        
        let wrapperView = UIView()
        wrapperView.backgroundColor = style.backgroundColor
        wrapperView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        wrapperView.layer.cornerRadius = style.cornerRadius
        
        if style.displayShadow {
            wrapperView.layer.shadowColor = UIColor.black.cgColor
            wrapperView.layer.shadowOpacity = style.shadowOpacity
            wrapperView.layer.shadowRadius = style.shadowRadius
            wrapperView.layer.shadowOffset = style.shadowOffset
        }
        
        
        progress = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        let progressSize  = CGSize(width: 50.0, height: 50.0)
        progress.frame = CGRect(x: style.horizontalPadding, y: style.verticalPadding, width: progressSize.width, height:progressSize.height)
        progress.startAnimating()
        
        var progressRect = CGRect.zero
        
        
        progressRect.origin.x = style.horizontalPadding
        progressRect.origin.y = style.verticalPadding
        progressRect.size.width = progress.bounds.size.width
        progressRect.size.height = progress.bounds.size.height
        
        
        messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.numberOfLines = style.messageNumberOfLines
        messageLabel.font = style.messageFont
     //   messageLabel.textAlignment = style.messageAlignment
        messageLabel.textAlignment = NSTextAlignment.center
        messageLabel.lineBreakMode = .byTruncatingTail;
        messageLabel.textColor = style.messageColor
        messageLabel.backgroundColor = UIColor.clear
        
        let maxMessageSize = CGSize(width: (self.bounds.size.width * style.maxWidthPercentage) - progressRect.size.width, height: self.bounds.size.height * style.maxHeightPercentage)
        let messageSize = messageLabel.sizeThatFits(maxMessageSize)
        let actualWidth = min(messageSize.width, maxMessageSize.width)
        let actualHeight = max(progressSize.height, min(messageSize.height, maxMessageSize.height))
        messageLabel.frame = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        
        var messageRect = CGRect.zero
        messageRect.origin.x = progressRect.origin.x + progressRect.size.width + style.horizontalPadding
        messageRect.origin.y = style.verticalPadding
        messageRect.size.width = messageLabel.bounds.size.width
        messageRect.size.height = messageLabel.bounds.size.height
        
        let longerWidth = messageRect.size.width
        let longerX = messageRect.origin.x
        
        let wrapperWidth = max((progressRect.size.width + (style.horizontalPadding * 2.0)), (longerX + longerWidth + style.horizontalPadding))
        let wrapperHeight = max((messageRect.origin.y + messageRect.size.height + style.verticalPadding), (progressRect.size.height + (style.verticalPadding * 2.0)))
        
        wrapperView.frame = CGRect(x: 0.0, y: 0.0, width: wrapperWidth, height: wrapperHeight)
        
        messageLabel.frame = messageRect
        wrapperView.addSubview(messageLabel)
        wrapperView.addSubview(progress)
        
       return wrapperView
    }
    
    public func showProgress(message:String)->UIView{
        let view = progressView(message: message)
        showToast(view)
        return view
    }
    
    
    public func showToast(_ progressView: UIView) {
        let position = self.centerPointForPosition(ToastPosition.center, toast: progressView)
        progressView.center = position
        self.addSubview(progressView)
    }
    
    private func centerPointForPosition(_ position: ToastPosition, toast: UIView) -> CGPoint {
        let padding: CGFloat = ToastManager.shared.style.verticalPadding
        
        switch(position) {
        case .top:
            return CGPoint(x: self.bounds.size.width / 2.0, y: (toast.frame.size.height / 2.0) + padding)
        case .center:
            return CGPoint(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
        case .bottom:
            return CGPoint(x: self.bounds.size.width / 2.0, y: (self.bounds.size.height - (toast.frame.size.height / 2.0)) - padding)
        }
    }
    
}
