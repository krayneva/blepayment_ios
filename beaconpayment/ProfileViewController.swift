//
//  ViewController.swift
//  beaconpayment
//
//  Created by Ирина on 05.08.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import UIKit
import MobileCoreServices
import ImagePicker

class ProfileViewController: BaseViewController
    , CardIOPaymentViewControllerDelegate
    , UITableViewDelegate
    , UITableViewDataSource
    , ImagePickerDelegate
    
{
    let DELETE_USER_DIALOG = 0;
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var addCardButton: CustomButton!
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var deletePhotoButton: UIButton!
    @IBOutlet weak var thumbsTable: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var deleteProfileButton: UIButton!
    
    

    var userID:Int = 0
    static var needToApplyBinding = false
    var photoPaths = [String]()
    var currentPhoto = 0
    let imagePicker = ImagePickerController()
    
    
    
    @IBAction func photoImageTapped(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.resetAssets()
            imagePicker.delegate = self
            imagePicker.imageLimit = 0
            imagePicker.galleryView.isHidden = true
            self.present(imagePicker, animated: true, completion:nil)
        }
        else{
            showToast("Камера не найдена")
        }
    }
    
  

    //callbacks from camera
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {}
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {}
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
        for image in images{
            let photoPath = ImageUtils.resizeImage(image: image)
            User.photosToAdd.append(photoPath)
        }
        currentPhoto = photoPaths.count
        setPhotoImages()
    }
    
    func applyBinding(){
        userID = User.getField(User.Fields.ID) as! Int
        showProgress(msg: "Прикрепление реккурентной связки...")
        RequestHelper().applyBinding(userID, listener: self)
        ProfileViewController.needToApplyBinding = false
    }
    
    @IBAction func addCardTapped(_ sender: Any) {
        let card = User.getField(User.Fields.CARD) as! String
        let userID = User.getField(User.Fields.ID) as! Int
        if !card.isEmpty {
            showProgress(msg: "Удаление карты...")
            RequestHelper().deleteUserCard(userID, listener: self)
        }
        else{
           /* let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)!
            cardIOVC.collectCardholderName = true
            cardIOVC.hideCardIOLogo = true
            cardIOVC.guideColor = UIColor.green
            cardIOVC.collectCVV = true
            present(cardIOVC, animated: true, completion: nil)*/
            let helper = RequestHelper()
            showProgress(msg: "Загрузка...")
            helper.registerOrder(userID, listener: self)
            
        }
    }
    
    override func initUI(){
        super.initUI()
        popToRootIfNoUser()
        userID = User.getField(User.Fields.ID) as! Int
        setPhotoImages()
        self.navigationController?.navigationBar.topItem?.title = "Профиль"
        let card = User.getField(User.Fields.CARD) as! String
        if !card.isEmpty {
            cardNumLabel.text = card
            addCardButton.setBackgroundImage(UIImage(named: "delete"), for: UIControlState.normal)
            addCardButton.setBackgroundImage(UIImage(named: "delete_gray"), for: UIControlState.disabled)
        }
        else{
            cardNumLabel.text = "Добавить карту"
            addCardButton.setBackgroundImage(UIImage(named: "add"), for: UIControlState.normal)
            addCardButton.setBackgroundImage(UIImage(named: "add_gray"), for: UIControlState.disabled)
        }
        nameLabel.text = "\(User.getField(User.Fields.SURNAME) as! String) \(User.getField(User.Fields.NAME) as! String)"
        phoneLabel.text = User.getField(User.Fields.PHONE) as? String
        photoImage.isUserInteractionEnabled = true
        self.navigationItem.hidesBackButton = true
        addSwipeRecognizers()
        if ProfileViewController.needToApplyBinding{
            applyBinding()
        }
     
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
    
    @IBAction func saveTapped(_ sender: Any) {
        if (userID != 0) {
            showProgress(msg: "Сохранение профиля...")
            RequestHelper().addAndRemoveUserPhotos(userID, listener: self)
        }
    }
    
    
    @IBAction func deleteProfileTapped(_ sender: Any) {
        showDialog(DELETE_USER_DIALOG, title: "Удаление профиля", message: "Вы действительно хотите удалить профиль?")
    }
    
    override func onDialogResponse(_ result: Bool, id: Int) {
        switch id {
        case 0:
            if result{
                BleHandler.sharedInstance.stopScanning()
                showProgress(msg: "Удаление профиля...")
                let userID = User.getField(User.Fields.ID) as! Int
                let helper = RequestHelper()
                helper.deleteUser(userID, listener: self)
            }
        default:
            break
        }
    }
    
    @IBAction func exitTapped(_ sender: Any) {
        BleHandler.sharedInstance.stopScanning()
        SignInData.clearData()
        User.clearData()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // cardIO disabled, not used func
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    // cardIO disabled, not used func
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo?, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\n Cardholders Name: %@ \n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.cardholderName, info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            let helper = RequestHelper()
            userID = User.getField(User.Fields.ID) as! Int
            helper.registerOrder(userID, listener: self)
        }
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        switch requestType{
        case .deleteUser:
            dismissProgress()
            SignInData.clearData()
            User.clearData()
            self.navigationController?.popToRootViewController(animated: true)
        case .addUser:
            dismissProgress()
            performSegue(withIdentifier: "HomeSegue", sender: nil)
        case .registerOrder:
            dismissProgress()
            performSegue(withIdentifier: "WebViewSegue", sender:nil)
        case .applyBinding, .deleteBinding:
            dismissProgress()
            initUI()
        case .addPhoto, .deletePhoto:
            break
        case .editUser, .loadPhoto:
            dismissProgress()
            initUI()
        default: break
            
        }
    }
    
    override func updateUIErrorReceived(requestType: RequestHelper.RequestType, error: ErrorResponse!) {
        super.updateUIErrorReceived(requestType: requestType, error: error)
        switch requestType{
        case .addPhoto:
            // define which photo is bad
            User.photosToAdd.remove(at: 0)
            initUI()
            showProgress(msg: "Сохранение профиля...")
            RequestHelper().addAndRemoveUserPhotos(userID, listener: self)
        default:
            break
        }
    }

    func setPhotoImages(){
        photoPaths.removeAll()
        let photos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        for  photo in photos {
            photoPaths.append(photo.fullPathURL!.path)
        }
        User.photosToAdd = Array(Set(User.photosToAdd).subtracting(User.photosToDelete))
        photoPaths.append(contentsOf: User.photosToAdd)
        photoPaths = Array(Set(photoPaths).subtracting(User.photosToDelete))
        setMainPhoto(newCurrentPhotoIndex: currentPhoto)
        setThumbnaiuls()
        if (User.photosToAdd.count==0) && (User.photosToDelete.count == 0){
            saveButton.isHidden = true
        }
        else{
            saveButton.isHidden = false
        }
    }
    
    func setMainPhoto(newCurrentPhotoIndex: Int){
        if !photoPaths.isEmpty{
            currentPhoto = newCurrentPhotoIndex
            if (currentPhoto == -1 || currentPhoto == photoPaths.count)  {
                currentPhoto = 0
            }
            
            if photoPaths.count>currentPhoto {
                photoImage.image = UIImage(named: photoPaths[currentPhoto])
            }
            else{
                // TODO set noimage pic
            }
        }
    }
    
    @IBAction func deletePhotoTapped(_ sender: Any) {
        if photoPaths.count>1{
            let path = photoPaths[currentPhoto]
            User.photosToDelete.append(path)
            ImageUtils.deleteFile(path: path)
            currentPhoto -= 1;
            setPhotoImages()
        }
        else{
            showToast("Нельзя удалить последнее фото")
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WebViewSegue"
        {
            if let destinationVC = segue.destination as? WebViewController {
                destinationVC.urlString = SignInData.formURL
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThumbCell", for: indexPath) as! ImageThumbnailRowView
        cell.imageThumb.image = UIImage(named: photoPaths[indexPath.row])
        cell.imageThumb.contentMode = .scaleAspectFit
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setMainPhoto(newCurrentPhotoIndex: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return photoPaths.count
    }
    
    func setThumbnaiuls(){
        thumbsTable.reloadData()
    }
    
    
    override func clearDataOnDismiss() {
        print ("clearing data on dismiss - photopaths, user.photostoadd, user.photostoadd")
        photoPaths.removeAll()
        User.photosToAdd.removeAll()
        for path in User.photosToAdd{
            ImageUtils.deleteFile(path: path)
        }
        User.photosToDelete.removeAll()
        currentPhoto = 0
        
    }
    

    override func onConnectivityChange(connected: Bool) {
        saveButton.isEnabled = connected
        exitButton.isEnabled = connected
        deleteProfileButton.isEnabled = connected
        addCardButton.isEnabled = connected
    }
    
    
}

