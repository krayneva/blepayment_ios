//
//  AgreementViewController.swift
//  beaconpayment
//
//  Created by Ирина on 27.03.17.
//  Copyright © 2017 sbt. All rights reserved.
//

import Foundation

class AgreementViewController: BaseViewController{
    @IBOutlet weak var buttonDecline: UIButton!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var textViewAgreement: UITextView!



    @IBAction func onDeclineTapped(_ sender: Any) {
        SignInData.clearData()
        User.clearData()
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onAcceptTapped(_ sender: Any) {
        performSegue(withIdentifier: "RegistrationSegue", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
        textViewAgreement.contentOffset.y = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.topItem?.title = "Пользовательское соглашение"
        
    }
}
