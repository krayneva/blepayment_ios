//
//  HomeViewController.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class HomeViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate//, UIScrollViewDelegate
 //   ,CLLocationManagerDelegate
{
    
    
  

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var beaconTable: UITableView!
    @IBOutlet weak var imagePhoto: UIImageView!

    
    
    override func initUI(){
        super.initUI()
        popToRootIfNoUser()
        let fullName = (User.getField(User.Fields.NAME) as! String)+(" ")+(User.getField(User.Fields.SURNAME) as! String)
        name.text = fullName
        setPhotoImages()
        addSwipeRecognizers()
        NotificationCenter.default.addObserver(self, selector: #selector(updateBeaconList), name: NSNotification.Name(rawValue: BleHandler.bleNotificationId), object: nil)
        self.navigationController?.navigationBar.topItem?.title = "Сбербанк Hands Free"
        self.navigationItem.hidesBackButton = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }



    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return BleHandler.beaconList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestSwiftCell")
        let event: Event = BleHandler.beaconList[indexPath.row]
        let merchantName = BleHandler.merchantList[event.beaconID]
        if (merchantName != nil){
            cell.textLabel?.text = merchantName
        }
        else{
            cell.textLabel?.text = "Major:\(event.major)    Minor:\(event.minor)"
        }
        let period = (Int64(Date().timeIntervalSince1970) - event.timestamp)
        //print("period is \(period)")
        //print("event timestamp is \(event.timestamp) current timestamp: \(Int64(NSDate().timeIntervalSince1970))")
        if period < 3 {
            cell.detailTextLabel?.text =  "online"
        }
        else{
            cell.detailTextLabel?.text =  "\(period)c"
        }
        if period>10{
            cell.detailTextLabel?.textColor = Colors.orderCancelled.get()
        }
        else{
            cell.detailTextLabel?.textColor = Colors.main.get()
        }
        return cell
    }
    

    func updateBeaconList(){
        beaconTable.reloadData()
    }
    
    
    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        switch requestType {
        case RequestHelper.RequestType.getMerchants:
            updateBeaconList()
        default:
            return
        }
    }
    
    func setPhotoImages(){
        var paths = [String]()
        let photos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        for  photo in photos {
            paths.append(photo.fullPathURL!.path)
        }
        if !paths.isEmpty{
            imagePhoto.image = UIImage(named: paths[0])
        }
    }
    
    
    override func onConnectivityChange(connected: Bool) {
        status.text = connected ? "В сети" : "Не в сети"
        status.textColor = connected ? Colors.main.get() : Colors.darkGray.get()
    }
}
