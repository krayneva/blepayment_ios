//
//  ResponseParser.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation

class ResponseParser{
    let TAG = "ResponseParser "
    
    enum Fields:String{
        //check phone response fields
        case user_id,registered, sms, photo, first_name, last_name, phone, binding, masked_card, type, sms_id, code, id, token, beacons, device
    }
    
    
    
    func parseJSONToDict(_ jsonString:NSString) -> Dictionary<String, AnyObject> {
        if let data: Data = jsonString.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: true){
            do{
                if let jsonObj = try JSONSerialization.jsonObject(
                    with: data,
                    options: JSONSerialization.ReadingOptions(rawValue: 0)) as? Dictionary<String, AnyObject>{
                    return jsonObj
                }
            }catch{
                print("Error while trying to parse result")
            }
        }
        return [String: AnyObject]()
    }
    
    func parseResult(_ data: Data?, request: NSMutableURLRequest, requestType: RequestHelper.RequestType, listener: UpdateUIProtocol?){
        if (requestType == RequestHelper.RequestType.loadPhoto){
            if (parseLoadPhoto(data:data, request:request, listener:listener)){
                if let listener = listener{
                    listener.updateUI(requestType: RequestHelper.RequestType.loadPhoto, data: data as AnyObject)
                }
            }
            return
        }
        let result = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)!
       
         print(self.TAG, "response is",result)
        var dict = parseJSONToDict(result)
        if let _ = dict["code"]{
            dict["requestBody"] = request.httpBody as AnyObject
            if let listener = listener{
                listener.updateUIErrorReceived(requestType: requestType, error: ErrorResponse(data: dict as AnyObject))
            }
        }
        else{
            var responseData:AnyObject? = dict as AnyObject?
            switch requestType{
            case .checkPhone:
               responseData = parseCheckPhone(dict, listener: listener)
            case .loadUser:
                responseData = parseLoadUser(dict, listener: listener)
            case .getSms:
                responseData = parseGetSms(dict, listener: listener)
            case .checkSmsCode,.photoAuth:
                responseData = parseSmsCode(dict, listener: listener)
            case .getPhotoURLs:
                responseData = parsePhotoURLs(dict, listener: listener)
            case .getMerchants:
                responseData = parseMerchants(dict, sentItems: request.httpBody, listener: listener)
            case .getOrders:
                responseData = parseOrders(dict, listener: listener)
            case.addUser,.editUser:
                responseData = parseAddUser(dict, listener: listener)
            case .registerOrder:
                responseData = parseRegisterOrder(dict, listener:listener)
            case .applyBinding:
                responseData = parseApplyBinding(dict, listener: listener)
            case .deleteBinding:
                responseData = parseDeleteBinding(dict, listener: listener)
            case .addPhoto:
                responseData = parseAddPhoto(dict, listener: listener)
            case .deletePhoto:
                responseData = parseDeletePhoto(dict, listener: listener)
            default:
                print(TAG+" unknown requestType, skip parsing result \(requestType)")
            }
            if let listener = listener{
                listener.updateUI(requestType: requestType, data: responseData)
            }
        }

    }
    
    
    func parseCheckPhone(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?) -> AnyObject?{
        let registered = dict[ResponseParser.Fields.registered.rawValue] as! Bool
        SignInData.registered = registered
        if registered{
            SignInData.userID = dict[ResponseParser.Fields.user_id.rawValue] as! Int
        }
        return dict as AnyObject
        //TODO save result to db if needed
    }
    
    func parseLoadUser(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?) -> AnyObject?{
        let name: String = dict[ResponseParser.Fields.first_name.rawValue] as! String
        let surname: String = dict[ResponseParser.Fields.last_name.rawValue] as! String
        User.setField(User.Fields.NAME, value: name as AnyObject)
        User.setField(User.Fields.SURNAME, value: surname as AnyObject)
        return dict as AnyObject
        //TODO save result to db if needed
    }

    
    func parseGetSms(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        let smsID: String = dict[ResponseParser.Fields.sms_id.rawValue] as! String
        SignInData.smsID = smsID
        return dict as AnyObject
    }
    
    func parseSmsCode(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)-> AnyObject?{
        //TODO analyze if it is registration or login - diff server answers
        if SignInData.registered{
            User.initFromDict(dict)
        }
        else{
            
        }
        
        return dict as AnyObject
        
    }
    
    
    
    func parsePhotoURLs(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        if let nodes = dict["photos"] as? NSArray{
            for node in nodes
            {
                let item = node as! NSDictionary
                let url = item["url"] as! String
                let filename = item["file_name"] as! String
                let id = item["id"] as! Int
                User.addPhoto(url: url, fileName: filename, id: id)
            }
        }
        return dict as AnyObject
    }
    
    
    func parseMerchants(_ dict: Dictionary<String, AnyObject>, sentItems: Data?, listener: UpdateUIProtocol?)->AnyObject?{
        for row in dict{
            BleHandler.merchantList[row.0] = String(describing: row.1)
        }
        
        if (sentItems != nil){
            let itemsDict = parseJSONToDict(NSString(data: sentItems!, encoding:String.Encoding.utf8.rawValue)!)
            let items = itemsDict["beacons"] as? NSArray
            if items != nil{
                for item in items!{
                    if BleHandler.merchantList[item as! String] == nil{
                        BleHandler.merchantList[item as! String] = "Наименование не задано"
                    }
                }
            }
        }
        return dict as AnyObject
    }
    
    func parseOrders(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
               return dict as AnyObject
    }
    
    
    func parseAddUser(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        User.initFromDict(dict)
        return dict as AnyObject
    }
    
    func parseLoadPhoto(data:Data?, request:NSMutableURLRequest, listener:UpdateUIProtocol?)->Bool{
        let userPhotos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        for photo in userPhotos {
            if photo.url == request.url{
                ImageUtils.saveToFile(data: data!, fileName: photo.fileName!)
            }
        }
        
        
        for photo in userPhotos {
            if !photo.isLoaded(){
                let helper = RequestHelper()
                helper.loadPhoto(url: photo.url!, listener: listener)
                return false
            }
        }
        return true
    }
    
    

    func parseRegisterOrder(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        SignInData.orderID = dict["order_id"] as! Int
        SignInData.formURL = dict["form_url"] as! String
        return dict as AnyObject
    
    }
    
    func parseApplyBinding(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        User.setField(User.Fields.CARD, value: dict["masked_card"]!)
        User.setField(User.Fields.CARD_DATE, value: dict["expiry_date"]!)
        SignInData.clearData()
        return dict as AnyObject
        
    }
    
    func parseDeleteBinding(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        User.clearField(User.Fields.CARD)
        User.clearField(User.Fields.CARD_DATE)
        SignInData.clearData()
        return dict as AnyObject
        
    }

    func parseAddPhoto(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        User.photosToAdd.remove(at: 0)
        User.addPhoto(url: dict["url"] as! String, fileName: dict["file_name"] as! String, id: dict["id"] as! Int)
        RequestHelper().addAndRemoveUserPhotos(User.getField(User.Fields.ID) as! Int, listener: listener!)
        return dict as AnyObject
    }
    
    func parseDeletePhoto(_ dict: Dictionary<String, AnyObject>, listener: UpdateUIProtocol?)->AnyObject?{
        User.removePhoto(path: User.photosToDelete[0])
        User.photosToDelete.remove(at: 0)
        RequestHelper().addAndRemoveUserPhotos(User.getField(User.Fields.ID) as! Int, listener: listener!)
        return dict as AnyObject
    }
    
    
   
}
