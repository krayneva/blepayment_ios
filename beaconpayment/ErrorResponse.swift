//
//  Error.swift
//  beaconpayment
//
//  Created by Ирина on 06.12.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
class ErrorResponse{
    var message, code: String
    
    init(data: AnyObject) {
        if let dict = data as? Dictionary<String,AnyObject>{
            message = dict["message"] as! String
            code = dict["code"] as! String
            }
        else{
            message = "Unknown error ocurred"
            code = "Unknown error"
        }
    }
    

    var localMessage: String {
        switch message {
            case "Terminal not found.": return "Терминал не найден"
            case "Beacon not found." : return "Маяк не найден"
            case "Merchant not found." : return "Мерчант не найден"
            case "No detected faces in the photos." : return "Не удалось распознать лицо"
            case "Unsupported file extension." : return "Неподдерживаемый формат файла"
            case "Identical users are not found." : return "Не найдено похожих пользователей"
            case "Order registration error in the RBS payment system." : return "Ошибка регистрации заказа РБС"
            case "Order payment error in the RBS payment system." : return "Ошибка оплаты заказа в системе РБС"
            case "No detected faces." : return "Не удалось распознать лицо"
            case "More that one face detected." : return "Распознано более одного лица"
            case "Face ID is null or empty." : return "Некорректный идентификатор лица"
            case "Person ID is null or empty." : return "Некорректный идентификатор пользоваетеля"
            case "Person is null." : return "Пользователь не найден"
            case "Failed detect face." : return "Не удалось распознать лицо"
            case "Failed add person face." : return "Не удалось добавить фотографию"
            case "Failed delete person face." : return "Не удалось удалить фотографию"
            case "Unsupported type of authorization." : return "Данный вид авторизации не поддерживается"
            case "Failed authenticate by photo." : return "Аутентификация по фото не удалась"
            case "Failed verify SMS code: Invalid code." : return "Неверный смс-код"
            case "User not found." : return "Пользователь не найден"
            case "PhotoNotFound" : return "Фотография не найдена"
            case "Phone number format is invalid." : return "Неверный формат номера телефона"
            case "Phone number is not registered." : return "Номер телефона не зарегистрирован"
            case "Phone number is already in use." : return "Пользователь с таким номером телефона уже зарегистрирован"
            case "Create user error." : return "Ошибка при создании пользователя"
            case "Update user error." : return "Ошибка при обновлении пользователя"
            case "Delete user error." : return "Ошибка при удалении пользователя"
            case "User is not identical." : return "Фото не прошло верификацию. Попробуйте еще раз"
            case "Delete user's photo error." : return "Ошибка при удалении фотографии пользователя"
            default: return "Произошла неизвестная ошибка"
        }
    }

}
