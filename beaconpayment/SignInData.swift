//
//  SignInData.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation


class SignInData{
    static var userID: Int = 0
    static var phone: String = ""
    static var registered: Bool = false
    static var smsID = "";
    static var bindingID = ""
    static var orderID = 0
    static var formURL = ""
    
    static func clearData(){
        userID = 0
        phone = ""
        registered = false
        smsID = ""
        bindingID = ""
        orderID = 0
        formURL = ""
    }
}
