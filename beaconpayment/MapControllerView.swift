//
//  MapControllerView.swift
//  beaconpayment
//
//  Created by Ирина on 29.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import MapKit

class MapControllerView: BaseViewController, MKMapViewDelegate{
    var lat: Double = 0.0, lon: Double = 0.0

    
    @IBOutlet weak var mapView: MKMapView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        popToRootIfNoUser()
        self.navigationController?.navigationBar.topItem?.title = "Карта"
        print("lat lon : \(lat)  \(lon)")
        let center = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = center
        mapView.addAnnotation(annotation)
    }
    
    
    
    
    }
