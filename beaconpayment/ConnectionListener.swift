//
//  ConnectionListener.swift
//  beaconpayment
//
//  Created by Ирина on 05.12.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import SystemConfiguration
import Foundation

class ConnectionListener{
    
    static let connectionNotificationId = "connectionNotification"
    
    static let sharedInstance = ConnectionListener()
    var reachability: Reachability
    
    init(){
        reachability = Reachability()!
    }
    
    func startMonitoringConnection(){
        reachability.whenReachable = { reachability in
             self.onNetworkConnected()
        }
        reachability.whenUnreachable = { reachability in
             self.onNetworkDisconnected()
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func isConnected() -> Bool{
        return reachability.isReachable
    }
    
    
    func stopMonitoringConnection(){
        reachability.stopNotifier()
    }
    
    
    func onNetworkConnected(){
    //    BleHandler.sharedInstance.sendAliveOrAllIn(caller: "network state changed")
        NotificationCenter.default.post(name: Notification.Name(rawValue: ConnectionListener.connectionNotificationId), object: nil, userInfo: ["connected": true])
    }
    
    func onNetworkDisconnected(){
          NotificationCenter.default.post(name: Notification.Name(rawValue: ConnectionListener.connectionNotificationId), object: nil, userInfo: ["connected": false])
    }

}
