//
//  ChildViewControllerDelegate.swift
//  beaconpayment
//
//  Created by Ирина on 30.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation

protocol ChildViewControllerDelegate{
    func childViewControllerResult(dict: Dictionary<String, AnyObject>)
}
