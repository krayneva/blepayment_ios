//
//  BleHandler.swift
//  beaconpayment
//
//  Created by Ирина on 10.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import CoreBluetooth


class BleHandler: NSObject, CLLocationManagerDelegate, UpdateUIProtocol, CBCentralManagerDelegate{
    
    
    static let SEND_EVENTS_PERIOD = 5.0
    static let SEND_ALIVE_PERIOD = 90.0
    static let SEND_ALL_IN_PERIOD = 90.0
    static let BEACON_LOST_PERIOD = 60*3
    
    var sendOutsTimer: Timer? = nil
    var sendAliveTimer: Timer? = nil
    var updateUITimer: Timer? = nil
    
    var lastAliveSent = Date()
    
    static var beaconList = [Event]()
    static var merchantList = Dictionary<String, String>()
    static let bleNotificationId: String = "bleNotification"
    
    static let logNotificationId: String = "logNotificationId"
    var locationManager: CLLocationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString: "F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "")
    
    
    
    class var sharedInstance :BleHandler {
        struct Singleton {
            static let instance = BleHandler()
        }
        
        return Singleton.instance
    }
    
    override init(){
        super.init()
        lastAliveSent = Date() - 200
    }
    
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("location updates are stopped")
        // stopScanning()
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
    }
    
    
    func startUIUpdate(){
        locationManager.startRangingBeacons(in: region)
        updateUITimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(BleHandler.notifyUI), userInfo: nil, repeats: true)
    }
    
    
    func stopUIUpdate(){
        updateUITimer?.invalidate()
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        switch central.state{
        case .poweredOff:
            BleHandler.beaconList.removeAll()
        default:
            break
        }
    }
    
    func turnOnBluetoothIfNeeded(){
        let centralManager = CBCentralManager(delegate: self, queue: nil, options:[CBCentralManagerOptionShowPowerAlertKey: true])
        switch centralManager.state{
        case .poweredOff:
            BleHandler.beaconList.removeAll()
        default:
            break
        }
      //  print ("central manager state is \(centralManager.state.rawValue)")
    }
    
    func startScanning(){
        turnOnBluetoothIfNeeded()
        let uuid = CBUUID(string: "F7826DA6-4FA2-4E98-8024-BC5B71E0893E")
        print("Ble scan started")
        let userID = User.getField(User.Fields.ID) as! Int
        if userID>0 {
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.delegate = self
            
            locationManager.pausesLocationUpdatesAutomatically = false
            region.notifyOnExit = true
            region.notifyOnEntry  = true
            
            locationManager.startMonitoring(for: region)
            locationManager.startRangingBeacons(in: region)
            sendOutsTimer = Timer.scheduledTimer(timeInterval: BleHandler.SEND_EVENTS_PERIOD, target: self, selector: #selector(BleHandler.sendOutEvents), userInfo: nil, repeats: true)
            sendAliveTimer = Timer.scheduledTimer(timeInterval: BleHandler.SEND_ALIVE_PERIOD, target: self, selector: #selector(BleHandler.sendAliveOrAllIn), userInfo: nil, repeats: true)
            sendAliveOrAllIn()
        }
    }
    
    func stopScanning(){
        print("Ble scan stopped")
        BleHandler.beaconList.removeAll()
        sendShutdown()
        locationManager.stopMonitoring(for: region)
        locationManager.stopRangingBeacons(in: region)
        
        sendAliveTimer?.invalidate()
        sendOutsTimer?.invalidate()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("updated location")
      //  RequestHelper().logToServer("updated location")
        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil, userInfo: ["message": "updated location"])
        sendAliveOrAllIn()
    }
    
    
    func locationManager(_ manager: CLLocationManager,didRangeBeacons beacons: [CLBeacon],in region: CLBeaconRegion){
        sendAliveOrAllIn()
        print("range received")
        for  beacon in beacons{
            let event = Event(b: beacon)
            if  BleHandler.beaconList.contains(event){
                let index = BleHandler.beaconList.index(of: event)
                BleHandler.beaconList[index!] = event
            }
            else{
                BleHandler.beaconList.append(event)
                var beacons: [Event] = []
                beacons.append(event)
                sendEvent(beacons, type: Event.eventType.In)
                /*   NSNotificationCenter.defaultCenter().postNotificationName(BleHandler.bleNotificationId, object: nil)
                 var notification = UILocalNotification()
                 notification.alertBody = "found beacon \(event.major) : \(event.minor)   \(getCurrrentTime())"
                 notification.soundName = "Default"
                 UIApplication.sharedApplication().presentLocalNotificationNow(notification)*/
            }
        }
        //  NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.bleNotificationId), object: nil, userInfo: ["message": "range received"])
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("did enter region")
        // locationManager.startRangingBeacons(in: region as! CLBeaconRegion)
       // locationManager.startUpdatingLocation()

        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,userInfo: ["message": "did enter region"])
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("did exit region")
        // locationManager.stopRangingBeacons(in: region as! CLBeaconRegion)
      //  locationManager.stopUpdatingLocation()
        BleHandler.beaconList.removeAll()
        //  sendAliveTimer?.invalidate()
        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,userInfo: ["message": "did exit region"])
    }
    
    
    
    //send out events if needed
    func sendOutEvents(){
        print ("send out events called")
        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,  userInfo: ["message": "send out called"])
        if (!ConnectionListener.sharedInstance.isConnected()){
            return
        }
        
        let helper = RequestHelper()
        var beacons: [String] = []
        
        for (i,ev) in BleHandler.beaconList.enumerated().reversed(){
            if (Int64(Date().timeIntervalSince1970) - ev.timestamp)>BleHandler.BEACON_LOST_PERIOD{
                beacons.append(ev.beaconID)
                BleHandler.beaconList.remove(at: i)
            }
        }
        
        
        
        if (beacons.count>0){
            helper.sendEvent(event: Event.eventType.Out, beacons: beacons)
        }
        else{
            if  BleHandler.beaconList.isEmpty {
                //sendOutsTimer?.invalidate()
            }
        }
        
        
    }
    
 /*  private func sendAllInEvents(caller: String){
       // print ("send all in events called from \(caller)")
       /* if (!ConnectionListener.sharedInstance.isConnected()){
            return
        }*/
         if (abs(lastAliveSent.timeIntervalSinceNow) > BleHandler.SEND_ALIVE_PERIOD-5){
            let helper = RequestHelper()
            var beacons: [String] = []
            
            for ev in BleHandler.beaconList {
                beacons.append(ev.beaconID)
            }
            if (beacons.count>0){
                RequestHelper().logToServer("send all ins reason: \(caller)")
                helper.sendEvent(event: Event.eventType.In, beacons: beacons)
            }
            else{
                RequestHelper().logToServer("send all ins : no beacons to send  from \(caller)")
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,  userInfo: ["message": "send all called"])
            lastAliveSent = Date()
         }
         else{
            print ("send all in -- to early  from \(caller)")
            NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,  userInfo: ["message": "attempt to send alive"])
        }
    }*/
    
    
    //send alive events
    func sendAliveOrAllIn(){
        if (!ConnectionListener.sharedInstance.isConnected()){
            RequestHelper().logToServer("device thinks its offline")
            return
        }
        if (abs(lastAliveSent.timeIntervalSinceNow) > BleHandler.SEND_ALIVE_PERIOD-5){
            print ("send alive called")
            if !((User.getField(User.Fields.ID) as! Int)==0) {
                let helper = RequestHelper()
            //    if (abs(lastAliveSent.timeIntervalSinceNow) > BleHandler.SEND_ALIVE_PERIOD+15){
                    var beacons: [String] = []
                    for ev in BleHandler.beaconList {
                        beacons.append(ev.beaconID)
                    }
                    helper.sendEvent(event: Event.eventType.In, beacons: beacons)
                    lastAliveSent = Date()
             //   }
              /*  else{
                    helper.sendEvent(event: Event.eventType.Alive, beacons: [] )
                }*/
                
            }
            else{
                RequestHelper().logToServer("service thinks user has logged out")

            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,  userInfo: ["message": "send alive called"])

        }
        else{
            //RequestHelper().logToServer("send in events called but its to early, last  is \(abs(lastAliveSent.timeIntervalSinceNow))")
            print ("send alive -- to early, last \(abs(lastAliveSent.timeIntervalSinceNow)) and period is \(BleHandler.SEND_ALIVE_PERIOD)")
            NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil,  userInfo: ["message": "attempt to send alive"])
        }
        
    }
    
    
    func sendEvent(_ events: [Event], type: Event.eventType){
        if (!ConnectionListener.sharedInstance.isConnected()){
            return
        }
        print ("send event called ")
        let helper = RequestHelper()
        var beacons: [String] = []
        for ev in events{
            beacons.append(ev.beaconID)
        }
        helper.sendEvent(event: type, beacons: beacons)
        
    }
    
    func sendShutdown(){
        if (!ConnectionListener.sharedInstance.isConnected()){
            return
        }
        print ("send shutdown called ")
        RequestHelper().sendEvent(event: .Shutdown, beacons: [])
        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.logNotificationId), object: nil)
    }
    
    
    func notifyUI(){
        let unknownBeacons = formBeaconIdsWithoutMerchants()
        if unknownBeacons.count>0{
            let helper = RequestHelper()
            helper.requestMerchants(beacons: unknownBeacons, listener: self)
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: BleHandler.bleNotificationId), object: nil)
    }
    
    // define if there are beacons of unknown merchant
    func formBeaconIdsWithoutMerchants()->[String]{
        var beacons: [String] = [String]()
        for ev in BleHandler.beaconList {
            if BleHandler.merchantList[ev.beaconID]==nil {
                beacons.append(ev.beaconID)
            }
        }
        return beacons
    }
    
    
    func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        
    }
    
    func updateUIErrorReceived(requestType: RequestHelper.RequestType, error: ErrorResponse!) {
        print(error.localMessage)
    }
    

    
    func getCurrrentTime()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter.string(from: Date())
    }
    
}
