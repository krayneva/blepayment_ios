//
//  PhotoAuthViewController.swift
//  beaconpayment
//
//  Created by Ирина on 24.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import MobileCoreServices
import UIKit
import AssetsLibrary
import Photos
import ImagePicker

class PhotoAuthViewController:BaseViewController,ImagePickerDelegate{

    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet var gesturer: UITapGestureRecognizer!
   // var uiImage: UIImage?
    var photoPath: String = ""
    let imagePicker = ImagePickerController()
    @IBOutlet weak var okButton: CustomButton!



    override func initUI() {
        super.initUI()
        imagePhoto.isUserInteractionEnabled = true
    }
    
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        if (!photoPath.isEmpty){
            showProgress(msg: "Обработка фотографии...")
            let helper = RequestHelper()
             helper.photoAuth(SignInData.userID, path:photoPath, listener: self)
        }
        else{
            showToast("Выберите фото")
        }
    }
    
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.imageLimit = 1
            imagePicker.doneButtonTitle = "Готово"
            imagePicker.galleryView.isHidden = true
            imagePicker.resetAssets()
            imagePicker.delegate = self

            self.present(imagePicker, animated: true, completion:nil)
        }
        else{
            showToast("Камера не найдена")
        }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
        photoPath = ImageUtils.resizeImage(image: images[0])
        imagePhoto.image = UIImage(contentsOfFile: photoPath)
    }
    
    //callbacks from camera
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {}
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {}
    
    
 /* 
 // old camera funcs
 public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           photoPath = ImageUtils.resizeImage(image: image)
            imagePhoto.image = UIImage(contentsOfFile: photoPath)
        }
        self.dismiss(animated: true, completion: nil)
    }
    

    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
 
 */

    override func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        dismissProgress()
        switch requestType{
            case RequestHelper.RequestType.photoAuth:
            if (SignInData.registered){
                showProgress(msg: "Загрузка фотографий...")
                RequestHelper().loadUserPhotoURLs(User.getField(User.Fields.ID) as! Int, listener: self)
                }
            case RequestHelper.RequestType.getPhotoURLs:
                RequestHelper().loadPhotos(User.getField(User.Fields.ID) as! Int, listener: self)
            case RequestHelper.RequestType.loadPhoto:
                BleHandler.sharedInstance.startScanning()
                performSegue(withIdentifier: "HomeSegue", sender: nil)
                ImageUtils.deleteFile(path: photoPath)
            default: break
        }
    }

    override func onConnectivityChange(connected: Bool) {
        okButton.isEnabled = connected
    }
}
