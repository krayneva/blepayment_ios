//
//  ScroollViewDelegate.swift
//  beaconpayment
//
//  Created by Ирина on 25.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit

class ScroollViewDelegate{
    
    var currentPos = 1
    var gesturer: UITapGestureRecognizer
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       // animateToItem(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
       /* if !decelerate{
            animateToItem(scrollView)
        }
 */
    }
    
    
    init(gest: UITapGestureRecognizer){
        gesturer = gest
    }
    
    func animateToItem(_ scrollView: UIScrollView){
        print("did scroll happened")
       /* let width = scrollView.frame.width
        print("width is \(width)")
        let remainder =  Int(scrollView.contentOffset.x.truncatingRemainder(dividingBy: width))
        print("remainder is \(remainder)")
        if remainder != 0 {
            let truncOffset = scrollView.contentOffset.x / width + ((remainder>Int(width/2)) ? width : 0)
            print("truncoffset is \(truncOffset))")
            scrollView.setContentOffset(CGPoint(x:truncOffset,y: 0), animated: true)
        }*/
    }

    
    func setPhotoImages(_ scrollView: UIScrollView, paths: [String]){
        
        let height = Int(scrollView.frame.height)
     //   let width = Int(scrollView.frame.width)
        var lastx = 0;
        for (i, path) in paths.enumerated() {
         //   let image = UIImageView()
            let image = UIButton()
            image.tag = i
            image.isUserInteractionEnabled = true
            image.contentMode = .scaleAspectFit
            let uiimage = UIImage(named: path)
            let scale = uiimage!.size.height/CGFloat(height)
            let width = Int(uiimage!.size.width / scale)
            let padding = 20
            image.frame = CGRect(x: lastx+padding, y: 0, width: width, height: height)
            lastx=lastx+width+padding
           // image.image = uiimage
            image.setImage(uiimage, for: UIControlState.normal)
            image.addGestureRecognizer(gesturer)
            scrollView.addSubview(image)
        }
        scrollView.contentSize = CGSize(width:lastx, height:height)
        scrollView.isScrollEnabled = true
        
    }

}
