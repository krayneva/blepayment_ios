//
//  BaseViewController.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import CoreLocation

class BaseViewController: UIViewController, UpdateUIProtocol, UINavigationControllerDelegate, UITextFieldDelegate{
   // @IBOutlet var progressView: UIView!
    
    let PROGRESS_TAG: Int = 110;
    var progressView: UIView?
    let locationManager = CLLocationManager()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateOnResume(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       self.navigationController?.navigationBar.topItem?.title = ""
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways) {
            locationManager.requestAlwaysAuthorization()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(onConnectivityChange(notification:)), name: NSNotification.Name(rawValue: ConnectionListener.connectionNotificationId), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUIPopToRoot(notification:)), name: NSNotification.Name(rawValue: HttpRequester.popToRootNotificationId), object: nil)
        initUI()
       
    }
    
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


    func initUI(){
          onConnectivityChange(connected: ConnectionListener.sharedInstance.isConnected())
    }
    
    func updateOnResume(notification: NSNotification){
        
        DispatchQueue.main.async {
            self.onConnectivityChange(connected: ConnectionListener.sharedInstance.isConnected())
            self.updateUIOnResume()
        }
    }
    
    func updateUIOnResume(){
        
    }
    
    // func for return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // func for touching outside
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let textField = touches.first?.view {
            textField.resignFirstResponder()
            self.view.endEditing(true)
        }
    }
    
    func showToast(_ message:String){
        print(message)
        self.view.makeToast(message, duration: 1.0, position: .center)
    }
    
    
    func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?) {
        
    }
    
    func updateUIErrorReceived(requestType: RequestHelper.RequestType, error: ErrorResponse!) {
        dismissProgress()
        showToast(error.localMessage)
    }
    
    func updateUIPopToRoot(notification: NSNotification) {
        popToRoot()
    }
    

    
    func showProgress(msg: String){
        progressView = view.showProgress(message: msg)
    }
    
    func dismissProgress(){
       progressView?.removeFromSuperview()
    }

    override func viewWillDisappear(_ animated: Bool) {
        if (self.isMovingFromParentViewController || self.isBeingDismissed){
            clearDataOnDismiss()
        }
    }
    
    deinit{
          NotificationCenter.default.removeObserver(self)
    }
    
    func showDialog(_ id: Int, title: String, message: String){
        let uiAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(uiAlert, animated: true, completion: nil)
    
        uiAlert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { action in
            self.onDialogResponse(true, id: id)
        }))
    
        uiAlert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { action in
           self.onDialogResponse(false, id: id)
        }))
        
    }
    
    func onDialogResponse(_ result: Bool, id: Int){}
    
    
    func clearDataOnDismiss(){}


    func addSwipeRecognizers(){
        let swipeGestureRight = UISwipeGestureRecognizer(target: self, action: "onSwipeRight:")
        swipeGestureRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeGestureRight)
        let swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: "onSwipeLeft:")
        swipeGestureLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeGestureLeft)
        
    }
    
    func onSwipeLeft(_ sender: Any) {
        if let parent =  self.parent as? CustomTabBarController{
            parent.onSwipeLeft(Any.self)
        }
    }
    
    func onSwipeRight(_ sender: Any) {
        if let parent =  self.parent as? CustomTabBarController{
            parent.onSwipeRight(Any.self)
        }
    }
    
    func onConnectivityChange(notification: NSNotification){
        let dict = notification.userInfo as! Dictionary<String, Bool>
        print("connection state: \(dict["connected"]!)")
        DispatchQueue.main.async(){
            self.onConnectivityChange(connected: dict["connected"]!)
        }
        
    }
    
    func onConnectivityChange(connected: Bool){
        
    }
    
    func popToRootIfNoUser(){
        if User.getField(User.Fields.ID) as! Int == 0 {
            popToRoot()
            }
        }
    

    func popToRoot(){
        if let nav = self.navigationController{
            print ("no user found, popped to root view controller")
            nav.popToRootViewController(animated: true)
        }
    }
}
