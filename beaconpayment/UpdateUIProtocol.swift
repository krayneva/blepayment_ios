//
//  UpdateUIProtocol.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation

protocol UpdateUIProtocol{
    
    func updateUI(requestType: RequestHelper.RequestType, data: AnyObject?)
    func updateUIErrorReceived(requestType: RequestHelper.RequestType, error:ErrorResponse!)
   
}
