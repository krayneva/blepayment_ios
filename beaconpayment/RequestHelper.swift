//
//  RequestHelper.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class RequestHelper{
    
    
 //   let serverAddr = "https://52.19.65.17/sberbank/api/v1/contactless"; // боевой
     let serverAddr = "https://52.50.183.84/sberbank/dev/api/v1/contactless"; // тестовый
    
    enum Urls: String{
        case user = "/users"
        case checkPhone = "/auth/check"
        case getSms = "/auth/sms/send"
        case checkSmsCode = "/auth/sms/verify"
        case photos = "/photos"
        case events = "/events"
        case merchants = "/merchants/find"
        case orders = "/orders"
        case photoAuth = "/auth/photo"
        case bindingsRegistration = "/bindings/registration"
        case bindings = "/bindings"
        case devices = "/devices"
        case log = "/push/test"
    }
    
    
    let url_users = "/users"
   
    
    enum RequestType: String {
        case checkPhone
        ,smsAuth
        ,photoAuth
        ,sendEvent
        ,loadPhoto
        ,loadPhotos
        ,addPhoto
        ,deletePhoto
        ,addUser
        ,deleteUser
        ,editUser
        ,getMerchants
        ,getOrders
        ,loadUser
        ,getSms
        ,checkSmsCode
        ,getPhotoURLs
        ,registerOrder
        ,applyBinding
        ,deleteBinding
        ,addDevice
        ,log
    }
    
    
    
    func checkPhoneNumber(_ phone :String, listener:UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.checkPhone.rawValue+"?"+User.Fields.PHONE+"=%2B7"+phone
        requester.requestGet(url, requestType: RequestType.checkPhone, listener: listener)
    }
    
    
    func getUser(_ userID :Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.user.rawValue+"/"+String(userID)
        requester.requestGet(url, requestType: RequestType.loadUser, listener: listener)
    }
    
    
    func getSms(_ userID :Int, phone: String, registered:Bool, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.getSms.rawValue
        var requestBody = Dictionary<String, AnyObject>()
        if (registered){
            requestBody[ResponseParser.Fields.user_id.rawValue] = SignInData.userID as AnyObject?
            requestBody[ResponseParser.Fields.type.rawValue] = "login" as AnyObject?
            requestBody[ResponseParser.Fields.device.rawValue] = Device().fullJSON as AnyObject?
        }
        else{
            requestBody[ResponseParser.Fields.type.rawValue] = "registration" as AnyObject?
        }
        
        requestBody[ResponseParser.Fields.phone.rawValue] = "+7"+SignInData.phone as AnyObject?
        requester.requestPost(url,requestBody: requestBody,  requestType: RequestType.getSms, listener: listener)
    }
    
    
    
    func checkSmsCode(sms_id: String,code: Int, registered:Bool, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.checkSmsCode.rawValue
        var requestBody = Dictionary<String, AnyObject>()
        if (registered){
            requestBody[ResponseParser.Fields.type.rawValue] = "login" as AnyObject?
        }
        else{
            requestBody[ResponseParser.Fields.type.rawValue] = "registration" as AnyObject?
        }
        requestBody[ResponseParser.Fields.sms_id.rawValue] = SignInData.smsID as AnyObject?
        requestBody[ResponseParser.Fields.code.rawValue] = code as AnyObject?
        requester.requestPost(url,requestBody: requestBody,  requestType: RequestType.checkSmsCode, listener: listener)
    }
    
    func loadUserPhotoURLs(_ userID: Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.user.rawValue+"/\(String(userID))"+Urls.photos.rawValue
        requester.requestGet(url, requestType: RequestType.getPhotoURLs, listener: listener)
    }
    
    func loadPhotos(_ userID: Int, listener: UpdateUIProtocol?){
        let userPhotos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        if userPhotos.isEmpty{
            if let listener = listener{
                listener.updateUI(requestType: RequestType.loadPhoto, data: nil)
            }
            return
        }
        for (i,photo) in userPhotos.enumerated() {
            if !photo.isLoaded(){
                let url = userPhotos[i].url
                loadPhoto(url:url!, listener: listener)
                return
            }
        }
        if let listener = listener{
            listener.updateUI(requestType: RequestType.loadPhoto, data: nil)
        }
        return
    }
    
    func loadPhoto(url: URL, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        requester.requestGet(url.absoluteString, requestType: RequestType.loadPhoto, listener: listener)
    }
    
    func photoAuth(_ userID: Int, data:UIImage, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.photoAuth.rawValue+"/\(userID)"
        requester.requestUpload(url, data:data, requestType: RequestType.photoAuth, listener: listener)

    }
    
    func photoAuth(_ userID: Int, path: String, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.photoAuth.rawValue+"/\(userID)"
        requester.requestUpload(url, path:path, requestType: RequestType.photoAuth, listener: listener)
        
    }
    
    func sendEvent(event: Event.eventType, beacons: [String]){
        let userID = User.getField(User.Fields.ID) as! Int
        if userID == 0{
            return
        }
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(describing:userID))"+Urls.events.rawValue
        var requestBody = Dictionary<String, AnyObject>()
        requestBody[ResponseParser.Fields.type.rawValue] = event.rawValue as AnyObject?
        if ((event==Event.eventType.In)||(event==Event.eventType.Out)){
            if !beacons.isEmpty {
            requestBody[ResponseParser.Fields.beacons.rawValue] = beacons as AnyObject?
            }
        }
        requester.requestPost(url, requestBody: requestBody, requestType: RequestType.sendEvent , listener: nil)
    }
    
    func requestMerchants(beacons: [String], listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.merchants.rawValue
        var requestBody = Dictionary<String, AnyObject>()
        requestBody[ResponseParser.Fields.beacons.rawValue] = beacons as AnyObject?
        requester.requestPost(url, requestBody: requestBody, requestType: RequestType.getMerchants , listener:listener)
    }
    
    func requestOrders(_ userID: Int, pageNum: Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr + Urls.user.rawValue+"/\(String(userID))"+Urls.orders.rawValue+(pageNum==1 ? "" : "?pageNumber=\(pageNum)")
      //  let url = serverAddr + Urls.user.rawValue+"/\(String(userID))\(Urls.orders.rawValue)?pageNumber=\(pageNum)&pageSize=1"
        requester.requestGet(url, requestType: RequestType.getOrders, listener: listener)
    }
    
    func addUser(name: String, surname: String, phone: String,listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue
        var requestBody = Dictionary<String, AnyObject>()
        requestBody[ResponseParser.Fields.first_name.rawValue] = name as AnyObject?
        requestBody[ResponseParser.Fields.last_name.rawValue] = surname as AnyObject?
        requestBody[ResponseParser.Fields.phone.rawValue] = phone as AnyObject?
        let device = Device()
        requestBody[Device.Fields.ID] = device.id as AnyObject
        requestBody[Device.Fields.pushKey] = device.pushKey as AnyObject?
        requestBody[Device.Fields.osType] = device.osType as AnyObject?
        requester.requestPost(url, requestBody: requestBody, requestType: RequestType.addUser, listener: listener)
    }
    
    func deleteUser(_ userID: Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(userID)"
        requester.requestDelete(url, requestType: RequestType.deleteUser, listener: listener)
    }
    
    func registerOrder(_ userID:Int, listener:UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))"+Urls.bindingsRegistration.rawValue+"/create"
        requester.requestPost(url, requestBody: Dictionary<String, AnyObject>(), requestType: RequestType.registerOrder, listener: listener)
    }
    
    func applyBinding(_ userID:Int, listener:UpdateUIProtocol?){
        let requester = HttpRequester()
        let orderID = SignInData.orderID
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))"+Urls.bindingsRegistration.rawValue+"/\(orderID)/apply"
        requester.requestPost(url, requestBody: Dictionary<String, AnyObject>(), requestType: RequestType.applyBinding, listener: listener)
    }
    
    
    func deleteUserCard(_ userID:Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))"+Urls.bindings.rawValue
        requester.requestDelete(url, requestType: RequestType.deleteBinding, listener: listener)
    }
    
    func addAndRemoveUserPhotos(_ userID: Int, listener: UpdateUIProtocol? ){
        let userPhotos = User.getField(User.Fields.PHOTOS) as! [UserPhoto]
        let photosToIgnore = Set(User.photosToAdd).intersection(Set(User.photosToDelete))
        
        User.photosToAdd = Array(Set(User.photosToAdd).subtracting(photosToIgnore))
        User.photosToDelete = Array(Set(User.photosToDelete).subtracting(photosToIgnore))

        if !User.photosToAdd.isEmpty{
            addUserPhoto(userID, path: User.photosToAdd[0], listener: listener)
            return
        }
        
        if !User.photosToDelete.isEmpty{
            for userPhoto in userPhotos {
                if userPhoto.fullPathURL?.path == User.photosToDelete[0]{
                    let photoID = userPhoto.id!
                    deleteUserPhoto(userID, photoID: photoID, listener: listener)
                    return
                }
            }
        }
        
        loadPhotos(userID, listener:listener)
        // removing photos
        // everything is done
       // listener.updateUI(requestType: RequestType.editUser, data: nil)
    }
   
    
    func addUserPhoto(_ userID: Int, path: String, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))"+Urls.photos.rawValue
        requester.requestUpload(url, path: path, requestType: RequestType.addPhoto, listener: listener)
    }
    
    func deleteUserPhoto(_ userID: Int, photoID: Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))\(Urls.photos.rawValue)/\(photoID)"
        requester.requestDelete(url, requestType: RequestType.deletePhoto, listener: listener)
    }

    
    func addUserDevice(_ userID: Int, listener: UpdateUIProtocol?){
        let requester = HttpRequester()
        let url = serverAddr+Urls.user.rawValue+"/\(String(userID))\(Urls.devices.rawValue)"
        var requestBody = Dictionary<String, AnyObject>()
        requestBody["device_id"] = UIDevice.current.identifierForVendor!.uuidString as AnyObject?
        requestBody["push_key"] = FIRInstanceID.instanceID().token() as AnyObject?
        requestBody["os_type"] = 2 as AnyObject?
        print ("addUserDevice")
        requester.requestPost(url, requestBody: requestBody, requestType: .addDevice, listener: listener)
    }

    
    func logToServer(_ text: String){
        /*
        let url = serverAddr+Urls.log.rawValue+"?text=\(text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
        HttpRequester().requestGet(url, requestType: .log, listener: nil)*/
    }
}
