//
//  TransactionRowView.swift
//  beaconpayment
//
//  Created by Ирина on 17.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit


class TransactionRowView: UITableViewCell{
    
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var amountText: UILabel!
    @IBOutlet weak var timeText: UILabel!

    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var merchantText: UILabel!
    @IBOutlet weak var geoButton: UIButton!


        
}
