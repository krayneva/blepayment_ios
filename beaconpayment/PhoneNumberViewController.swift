//
//  PhoneNumberViewController.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class PhoneNumberViewController : BaseViewController{
    
    @IBOutlet weak var phoneEdit: UITextField!
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    

    
    override func initUI(){
        super.initUI()
        let userID = User.getField(User.Fields.ID) as! Int
        if userID>0{
            performSegue(withIdentifier: "HomeSegue", sender: nil)
        }
        
        //   phoneEdit.text = "9500254584"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            versionLabel.text = "Версия \(version)"
        }
        else{
            versionLabel.text = ""
        }
    }
    
    @IBAction func okButtonClick(_ sender: AnyObject) {

        if let token = FIRInstanceID.instanceID().token(){
            print ("---token is \(token)")
        }
        else{
            print ("---token is nil")
        }


        let phone = phoneEdit.text!
        if phone.isEmpty{
            showToast("Введите номер телефона")

        }
        else{
            let requester = RequestHelper()
            SignInData.phone = phone
            showProgress(msg: "Загрузка...")
            requester.checkPhoneNumber(phone, listener: self)
            
        }
       
    }
    
    override func updateUI(requestType: RequestHelper.RequestType, data:AnyObject?) {
        dismissProgress()
        let dict = data as! Dictionary<String, AnyObject>
        if (SignInData.registered) {
            SignInData.userID = dict[ResponseParser.Fields.user_id.rawValue] as? Int ?? 0
            let smsAuth = dict[ResponseParser.Fields.sms.rawValue] as? Bool  ?? false
            let photoAuth = dict[ResponseParser.Fields.photo.rawValue] as? Bool ?? false
            if (smsAuth && photoAuth){
                 performSegue(withIdentifier: "ChooseAuthTypeSegue", sender: nil)
            }
            else{
                 performSegue(withIdentifier: "SmsAuthSegue", sender: nil)
            }
        }
        else{
            performSegue(withIdentifier: "SmsAuthSegue", sender: nil)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        phoneEdit.text = ""
    }
    
    override func onConnectivityChange(connected: Bool) {
        okButton.isEnabled = connected
    }
}

