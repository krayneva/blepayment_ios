//
//  Event.swift
//  beaconpayment
//
//  Created by Ирина on 14.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreLocation
import UIKit

class Event: Equatable{
    let beaconID: String
    let timestamp: Int64
    let uuid,major, minor:String
    let beacon: CLBeacon
    var proximity: CLProximity
    var proximityString: String
    
    enum eventType: String {
        case  In = "in"
        case Out = "out"
        case Alive = "alive"
        case Shutdown = "shutdown"
    }

    
    
    init(b: CLBeacon){
        uuid = b.proximityUUID.uuidString
        major = String(describing: b.major)
        minor = String(describing: b.minor)
        beaconID = "\(uuid):\(major):\(minor)".lowercased()
        timestamp =  Int64(Date().timeIntervalSince1970)
        beacon = b
        proximity = b.proximity
        proximityString = ""
        proximityString = getProximityString(proximity)
    }
    
    
    
    
    func getProximityString(_ prox: CLProximity)->String{
        switch prox
        {
        case .immediate:
        return "Близко"
        case .near:
        return "Недалеко"
        case .far:
        return"Далеко"
        case .unknown:
        return "Unknown"
            
        }
        
    }
        
    
   
}

func ==(lhs: Event, rhs: Event) -> Bool {
    return lhs.beaconID.compare(rhs.beaconID).rawValue==0
}
