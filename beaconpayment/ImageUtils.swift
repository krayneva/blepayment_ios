//
//  ImageUtils.swift
//  beaconpayment
//
//  Created by Ирина on 23.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils{
    static let maxWidth: CGFloat = 1024
    
    
    static func saveToFile(data: Data, fileName: String)->String?{
        let filename = getFullPath(fileName: fileName)
        let options = Data.WritingOptions.atomicWrite
        if (try? data.write(to: filename, options: options)) != nil {
            return filename.path
        }
        return nil
    }
    
    static func saveToFile(data: Data)->String?{
        let fileName = String(Date().timeIntervalSince1970)
        return saveToFile(data: data, fileName: fileName)
    }
    
    private static func save(data: Data, fileName: String){
        
    }
    
    static func getFilesDir() -> URL {
        let documentsDirectory = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first
        return documentsDirectory!
      
    }
    
    static func getFullPath(fileName: String)->URL{
        return getFilesDir().appendingPathComponent(fileName)
    }
    
    static func getTimeStampString()-> String{
    //  return  String(Date().timeIntervalSince1970)
        return "temp"
    }
    
    static func resizeImage(image: UIImage) -> String {
         print ("resizing image size is \(image.size)")
        let scale =   max (image.size.height/maxWidth, image.size.width/maxWidth)
        print ("resizing image scale is \(scale)")
        let newHeight = Int(image.size.height / scale)
        let newWidth = Int(image.size.width / scale)
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        print ("resizing image to \(newImage?.size.height)   \(newImage?.size.width)")
        return saveToFile(data: UIImagePNGRepresentation(newImage!)!)!
        
    }
    
    static func deleteFile(path: String){
        print ("deleting file \(path)")
        let fileManager = FileManager.default
        do{
            try fileManager.removeItem(atPath: path)
        }
        catch{
            print("error while trying to delete file \(path)")
        }
    }
    
    static func clearAssets(){
        let fileManager = FileManager.default

        do{
            let filePaths = try fileManager.contentsOfDirectory(atPath: getFilesDir().path)
            for path in filePaths{
                try fileManager.removeItem(atPath: getFullPath(fileName: path).path)
            }
        }
        catch{
            print ("error while trying to clean assets directory")
        }
    }
}
