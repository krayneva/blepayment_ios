//
//  Device.swift
//  beaconpayment
//
//  Created by Ирина on 26.12.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import Firebase


class Device{
    struct Fields{
        static let ID = "device_id"
        static let pushKey = "push_key"
        static let osType = "os_type"
    }
    
    var id: String {
        get{
            return UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    var pushKey: String?{
        get{
            return FIRInstanceID.instanceID().token()
        }
    }
    let osType:String = "2"
    
    
    var fullJSON: Dictionary<String, AnyObject> {
        get{
            var dict = Dictionary<String, AnyObject>()
            dict[Fields.ID] = id as AnyObject
            if let pushKeyUnwrapped = pushKey{
                dict[Fields.pushKey] = pushKeyUnwrapped as AnyObject
            }
            dict[Fields.osType] = 2 as AnyObject
            return dict
        }
    }
    

}
