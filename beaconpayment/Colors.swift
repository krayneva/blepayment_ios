//
//  Colors.swift
//  beaconpayment
//
//  Created by Ирина on 22.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
import UIKit


    public enum Colors {
        case transparent
        case main
        case orderSuccess
        case orderRegistered
        case orderCancelled
        case orderError
        case darkGray
        
    func get()->UIColor{
            switch self{
            case .main, .orderSuccess:
                return UIColor.mainColor
            case .orderError:
                return  UIColor.glnTomato
            case .orderCancelled:
                return UIColor.glnOrangeYellow
            case .darkGray:
                return UIColor.glnCoolGrey
            default:
                return UIColor.black
            }
        }
    }

extension UIColor {

    class var glnWarmGrey: UIColor {
        return UIColor(white: 112.0 / 255.0, alpha: 1.0)
    }
    
    class var glnMelon: UIColor {
        return UIColor(red: 1.0, green: 120.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
    }
    
    class var glnAzure: UIColor {
        return UIColor(red: 15.0 / 255.0, green: 161.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    class var glnVermillion: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 32.0 / 255.0, blue: 19.0 / 255.0, alpha: 1.0)
    }
    
    class var glnLightGold: UIColor {
        return UIColor(red: 1.0, green: 199.0 / 255.0, blue: 81.0 / 255.0, alpha: 1.0)
    }
    
    class var glnFreshGreen: UIColor {
        return UIColor(red: 74.0 / 255.0, green: 205.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0)
    }
    
    class var glnDarkGrey: UIColor {
        return UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 31.0 / 255.0, alpha: 1.0)
    }
    
    class var glnBlack: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    
    class var glnOrangeYellow: UIColor {
        return UIColor(red: 1.0, green: 165.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    class var glnDarkMint: UIColor {
        return UIColor(red: 77.0 / 255.0, green: 206.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)
    }
    
    class var glnSilver: UIColor {
        return UIColor(red: 193.0 / 255.0, green: 207.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0)
    }
    
    class var glnCoolGrey: UIColor {
        //A4 A9 AB
        return UIColor(red: 164.0 / 255.0, green: 169.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
    }
    
    class var glnAquaGreen: UIColor {
        return UIColor(red: 24.0 / 255.0, green: 217.0 / 255.0, blue: 147.0 / 255.0, alpha: 1.0)
    }
    
    class var glnBlack90: UIColor {
        return UIColor(white: 0.0, alpha: 0.9)
    }
    
    class var glnDarkNavyBlue: UIColor {
        return UIColor(red: 0.0, green: 4.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
    }
    
    class var glnTomato: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 71.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
    }
    
    class var glnGreenTeal: UIColor {
        return UIColor(red: 17.0 / 255.0, green: 191.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
    }
    
    class var glnBluishGreen: UIColor { 
        return UIColor(red: 21.0 / 255.0, green: 177.0 / 255.0, blue: 86.0 / 255.0, alpha: 1.0)
    }
    
    class var mainColor: UIColor {
        return UIColor(red: 47.0 / 255.0, green:146.0 / 255.0, blue: 4.0 / 255.0, alpha: 1.0)
    }
}
