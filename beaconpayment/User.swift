//
//  User.swift
//  beaconpayment
//
//  Created by Ирина on 09.11.16.
//  Copyright © 2016 sbt. All rights reserved.
//

import Foundation
class User{
    struct Fields{
        static let ID = "id"
        static let NAME = "first_name"
        static let SURNAME = "last_name"
        static let PHONE = "phone"
        static let TOKEN = "token"
        static let BINDING = "binding"
        static let CARD = "masked_card"
        static let CARD_DATE = "expiry_date"
        static let PHOTOS = "photos"
    }
    
    static var photosToDelete = [String]()
    static var photosToAdd = [String]()
    
   static func getField (_ field:String) -> AnyObject{
        if let returnValue :AnyObject = UserDefaults.standard.object(forKey: field) as AnyObject?
        {
            if (field==Fields.PHOTOS){
                return UserPhoto.unarchivePhoto(data: returnValue as! NSData) as AnyObject
            }
            
            return returnValue
        }
        else {
            if field == Fields.ID {
                return 0 as AnyObject
            }
            if field == Fields.PHOTOS{
                let array = [UserPhoto]()
                setField(Fields.PHOTOS, value: UserPhoto.archivePhotos(photos: array) as AnyObject)
                return array as AnyObject
            }
            return "" as AnyObject;
        }
    }
    
    static func setField(_ field: String, value: AnyObject){
        UserDefaults.standard.set(value, forKey: field)
        UserDefaults.standard.synchronize()

    }
    
    static func clearField(_ field: String){
        UserDefaults.standard.removeObject(forKey: field)
        UserDefaults.standard.synchronize()

    }
    
    static func addPhoto(url: String, fileName: String, id: Int){
        var photoArray = getField( Fields.PHOTOS) as! [UserPhoto]
        let photo = UserPhoto(url:url, fileName: fileName, id: id)
        photoArray.append(photo)
        setField(Fields.PHOTOS, value: UserPhoto.archivePhotos(photos: photoArray) as AnyObject)
    }

    static func removePhoto(path: String){
        var photoArray = getField(Fields.PHOTOS) as! [UserPhoto]
        for (i,photo) in photoArray.enumerated(){
            if path==photo.fullPathURL?.path {
                ImageUtils.deleteFile(path: path)
                photoArray.remove(at: i)
                setField(Fields.PHOTOS, value: UserPhoto.archivePhotos(photos: photoArray) as AnyObject)
                break
            }
        }
    }
    
    static func clearData(){
        let appDomain = Bundle.main.bundleIdentifier!
        clearPendingPhotos()
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        ImageUtils.clearAssets()
        BleHandler.merchantList.removeAll()
    }

    static func initFromDict(_ dict:Dictionary<String, AnyObject>){
        User.setField(User.Fields.ID, value: dict[ResponseParser.Fields.id.rawValue] as! Int as AnyObject)
        User.setField(User.Fields.NAME, value: dict[ResponseParser.Fields.first_name.rawValue] as! String as AnyObject)
        User.setField(User.Fields.SURNAME, value: dict[ResponseParser.Fields.last_name.rawValue] as! String as AnyObject)
        User.setField(User.Fields.PHONE, value: dict[ResponseParser.Fields.phone.rawValue] as! String as AnyObject)
        User.setField(User.Fields.TOKEN, value: dict[ResponseParser.Fields.token.rawValue] as! String as AnyObject)
        let binding = dict[ResponseParser.Fields.binding.rawValue] as! Bool
        User.setField(User.Fields.BINDING, value: binding as AnyObject)
        if binding{
            User.setField(User.Fields.CARD, value: dict[ResponseParser.Fields.masked_card.rawValue] as! String as AnyObject)
        }   
    }
    
    static func clearPendingPhotos(){
        photosToAdd.removeAll()
        photosToDelete.removeAll()
    }
}

